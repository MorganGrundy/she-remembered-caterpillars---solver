#ifndef CREATURETEST_H
#define CREATURETEST_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include <vector>
#include <optional>
#include <algorithm>

#include "colour.h"
#include "connection.h"
#include "island.h"
#include "creature.h"
#include "solver.h"
#include "path.h"
#include "action.h"
#include "pathchecker.h"

using namespace testing;

class CreatureTest: public ::testing::Test
{
protected:
    CreatureTest()
    {
        //Setup islands
        endIsland->destinations = 1;

        //Setup creature
        startIsland->creatures.push_back(creature);

        //Store in world
        world.islands.push_back(startIsland);
        world.islands.push_back(endIsland);
    }

    void SetUp() override { }

    void TearDown() override { }

    ~CreatureTest() override { }

    Solver world;
    const std::shared_ptr<Island> startIsland = std::make_shared<Island>();
    const std::shared_ptr<Island> endIsland = std::make_shared<Island>();
    const std::shared_ptr<Creature> creature =
            std::make_shared<Creature>(Colour::Invalid, startIsland);
};

TEST_F(CreatureTest, creatureSplit)
{
    //Create gate
    const auto gate = std::make_shared<Gate>(startIsland, endIsland, Colour::Invalid);
    gate->updateIslands();
    world.connections.push_back(gate);
    //For all base colour gates
    for (auto gateIt = ColourHelper::baseBegin, gateEnd = ColourHelper::baseEnd; gateIt != gateEnd;
         ++gateIt)
    {
        gate->colour = *gateIt;
        //For all composite colour creatures that contain gate colour
        for (auto colourIt = ColourHelper::baseBegin, colourEnd = ColourHelper::baseEnd;
             colourIt != colourEnd; ++colourIt)
        {
            if (*colourIt != *gateIt)
            {
                const Colour creatureColour = ColourHelper::combine(*colourIt, *gateIt);
                creature->colour = creatureColour;

                //Run solver
                const std::optional<Path> result = world.run();
                const std::string message = "Creature colour is " +
                        ColourHelper::toString(creatureColour) + ", Gate colour is " +
                        ColourHelper::toString(*gateIt);
                EXPECT_TRUE(result) << message;
                if (result)
                    PathChecker::checkPath(result.value(),
                    {ActionType::Split, ActionType::Connection}, message);
            }
        }
    }
    //Remove gate
    world.connections.clear();
    startIsland->connections.clear();
    endIsland->connections.clear();
}

TEST_F(CreatureTest, creatureJoin)
{
    //Create bridge
    const auto bridge = std::make_shared<Bridge>(startIsland, endIsland, Colour::Invalid);
    bridge->updateIslands();
    world.connections.push_back(bridge);
    //Create creature
    const auto otherCreature = std::make_shared<Creature>(Colour::Invalid, startIsland);
    startIsland->creatures.push_back(otherCreature);
    //For all composite colour bridges
    for (auto bridgeIt = ColourHelper::compositeBegin, bridgeEnd = ColourHelper::compositeEnd;
         bridgeIt != bridgeEnd; ++bridgeIt)
    {
        bridge->colour = *bridgeIt;
        //Set creature colours to parts of composite
        const auto colourParts = ColourHelper::split(*bridgeIt);
        creature->colour = colourParts->first;
        otherCreature->colour = colourParts->second;

        //Run solver
        const std::optional<Path> result = world.run();
        const std::string message = "Creature colours are " +
                ColourHelper::toString(colourParts->first) + " and " +
                ColourHelper::toString(colourParts->second) + ", Bridge colour is " +
                ColourHelper::toString(*bridgeIt);
        EXPECT_TRUE(result) << message;
        if (result)
            PathChecker::checkPath(result.value(), {ActionType::Join, ActionType::Connection},
                                   message);
    }
    //Remove bridge and creature
    world.connections.clear();
    startIsland->connections.clear();
    endIsland->connections.clear();
    startIsland->creatures.erase(startIsland->creatures.cend());
}

#endif // CREATURETEST_H
