#ifndef GATETEST_H
#define GATETEST_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include <vector>
#include <optional>

#include "colour.h"
#include "connection.h"
#include "island.h"
#include "creature.h"
#include "solver.h"
#include "path.h"
#include "action.h"
#include "pathchecker.h"

using namespace testing;

class GateTest: public ::testing::Test
{
protected:
    GateTest()
    {
        //Setup islands
        endIsland->destinations = 1;

        //Setup creature
        startIsland->creatures.push_back(creature);

        //Setup connections
        gate->updateIslands();

        //Store in world
        world.islands.push_back(startIsland);
        world.islands.push_back(endIsland);
        world.connections.push_back(gate);
    }

    void SetUp() override { }

    void TearDown() override { }

    ~GateTest() override { }

    Solver world;
    const std::shared_ptr<Island> startIsland = std::make_shared<Island>();
    const std::shared_ptr<Island> endIsland = std::make_shared<Island>();
    const std::shared_ptr<Gate> gate =
            std::make_shared<Gate>(startIsland, endIsland, Colour::Invalid);
    const std::shared_ptr<Creature> creature =
            std::make_shared<Creature>(Colour::Invalid, startIsland);
};

TEST_F(GateTest, uncolouredCreatureCanCrossGate)
{
    creature->colour = Colour::Uncoloured;
    for (const auto colour: ColourHelper::Colours)
    {
        //Set gate colour
        gate->colour = colour;

        //Run solver
        const std::optional<Path> result = world.run();
        const std::string message = "Gate colour is " + ColourHelper::toString(colour);
        EXPECT_TRUE(result) << message;
        if (result)
            PathChecker::checkPath(result.value(), {ActionType::Connection}, message);
    }
}

TEST_F(GateTest, creatureCannotCrossSameColourGate)
{
    for (auto it = ColourHelper::baseBegin, end = ColourHelper::compositeEnd; it != end; ++it)
    {
        //Set creature colour
        creature->colour = *it;
        //Set gate colour
        gate->colour = *it;

        //Run solver
        const std::optional<Path> result = world.run();
        EXPECT_FALSE(result) << "Colour is " << ColourHelper::toString(*it);
    }
}

TEST_F(GateTest, baseCreatureCanCrossDifferentBaseGate)
{
    for (auto creatureIt = ColourHelper::baseBegin, creatureEnd = ColourHelper::baseEnd;
         creatureIt != creatureEnd; ++creatureIt)
    {
        for (auto gateIt = ColourHelper::baseBegin, gateEnd = ColourHelper::baseEnd;
             gateIt != gateEnd; ++gateIt)
        {
            if (*creatureIt != *gateIt)
            {
                //Set creature colour
                creature->colour = *creatureIt;
                //Set gate colour
                gate->colour = *gateIt;

                //Run solver
                const std::optional<Path> result = world.run();
                const std::string message = "Creature colour is " +
                        ColourHelper::toString(*creatureIt) + ", Gate colour is " +
                        ColourHelper::toString(*gateIt);
                EXPECT_TRUE(result) << message;
                if (result)
                    PathChecker::checkPath(result.value(), {ActionType::Connection}, message);
            }
        }
    }
}

TEST_F(GateTest, compositeCreatureCannotCrossPartOfCompositeBaseGate)
{
    endIsland->destinations = 2;
    for (auto it = ColourHelper::compositeBegin, end = ColourHelper::compositeEnd; it != end; ++it)
    {
        const auto creatureColourParts = ColourHelper::split(*it);
        const auto gateColours = {creatureColourParts->first, creatureColourParts->second};
        for (const Colour gateColour: gateColours)
        {
            //Set creature colour
            creature->colour = *it;
            //Set gate colour
            gate->colour = gateColour;

            //Run solver
            const std::optional<Path> result = world.run();
            EXPECT_FALSE(result) << "Creature colour is " <<
                                                ColourHelper::toString(*it) <<
                                                ", Gate colour is " <<
                                                ColourHelper::toString(gateColour);
        }
    }
    endIsland->destinations = 1;
}

TEST_F(GateTest, compositeCreatureCanCrossNotPartOfCompositeBaseGate)
{
    for (auto creatureIt = ColourHelper::compositeBegin, creatureEnd = ColourHelper::compositeEnd;
         creatureIt != creatureEnd; ++creatureIt)
    {
        const auto creatureColourParts = ColourHelper::split(*creatureIt);
        for (auto gateIt = ColourHelper::baseBegin, gateEnd = ColourHelper::baseEnd;
             gateIt != gateEnd; ++gateIt)
        {
            if (*gateIt != creatureColourParts->first && *gateIt != creatureColourParts->second)
            {
                //Set creature colour
                creature->colour = *creatureIt;
                //Set gate colour
                gate->colour = *gateIt;

                //Run solver
                const std::optional<Path> result = world.run();
                const std::string message = "Creature colour is " +
                        ColourHelper::toString(*creatureIt) + ", Gate colour is " +
                        ColourHelper::toString(*gateIt);
                EXPECT_TRUE(result) << message;

                if (result)
                    PathChecker::checkPath(result.value(), {ActionType::Connection}, message);
            }
        }
    }
}

TEST_F(GateTest, baseCreatureCannotCrossCompositeOfPartCompositeGate)
{
    for (auto gateIt = ColourHelper::compositeBegin, gateEnd = ColourHelper::compositeEnd;
         gateIt != gateEnd; ++gateIt)
    {
        const auto gateColourParts = ColourHelper::split(*gateIt);
        const auto creatureColours = {gateColourParts->first, gateColourParts->second};
        for (const Colour creatureColour: creatureColours)
        {
            //Set creature colour
            creature->colour = creatureColour;
            //Set gate colour
            gate->colour = *gateIt;

            //Run solver
            const std::optional<Path> result = world.run();
            EXPECT_FALSE(result) << "Creature colour is " <<
                                                ColourHelper::toString(creatureColour) <<
                                                ", Gate colour is " <<
                                                ColourHelper::toString(*gateIt);
        }
    }
}

TEST_F(GateTest, baseCreatureCanCrossCompositeNotOfPartCompositeGate)
{
    for (auto gateIt = ColourHelper::compositeBegin, gateEnd = ColourHelper::compositeEnd;
         gateIt != gateEnd; ++gateIt)
    {
        const auto gateColourParts = ColourHelper::split(*gateIt);
        for (auto creatureIt = ColourHelper::baseBegin, creatureEnd = ColourHelper::baseEnd;
             creatureIt != creatureEnd; ++creatureIt)
        {
            if (*creatureIt != gateColourParts->first && *creatureIt != gateColourParts->second)
            {
                //Set creature colour
                creature->colour = *creatureIt;
                //Set gate colour
                gate->colour = *gateIt;

                //Run solver
                const std::optional<Path> result = world.run();
                const std::string message = "Creature colour is " +
                        ColourHelper::toString(*creatureIt) + ", Gate colour is " +
                        ColourHelper::toString(*gateIt);
                EXPECT_TRUE(result) << message;
                if (result)
                    PathChecker::checkPath(result.value(), {ActionType::Connection}, message);
            }
        }
    }
}

TEST_F(GateTest, compositeCreatureCannotCrossDifferentCompositeBridge)
{
    endIsland->destinations = 2;
    for (auto creatureIt = ColourHelper::compositeBegin, creatureEnd = ColourHelper::compositeEnd;
         creatureIt != creatureEnd; ++creatureIt)
    {
        for (auto gateIt = ColourHelper::compositeBegin, gateEnd = ColourHelper::compositeEnd;
             gateIt != gateEnd; ++gateIt)
        {
            if (*creatureIt != *gateIt)
            {
                //Set creature colour
                creature->colour = *creatureIt;
                //Set gate colour
                gate->colour = *gateIt;

                //Run solver
                const std::optional<Path> result = world.run();
                EXPECT_FALSE(result) << "Creature colour is " <<
                                                    ColourHelper::toString(*creatureIt) <<
                                                    ", Bridge colour is " <<
                                                    ColourHelper::toString(*gateIt);
            }
        }
    }
    endIsland->destinations = 1;
}

#endif // GATETEST_H
