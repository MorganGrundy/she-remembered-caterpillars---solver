include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += c++17 console
CONFIG -= app_bundle
CONFIG += thread
CONFIG += qt

HEADERS += \
		../src/connection.h \
		../src/creature.h \
		../src/island.h \
		../src/solver.h \
		../src/colour.h \
		../src/path.h \
		../src/action.h \
		activatorbridgetest.h \
		bridgetest.h \
		creaturetest.h \
		gatetest.h \
		paintertest.h \
		pathchecker.h \
		switchbridgetest.h

SOURCES += \
	../src/connection.cpp \
	../src/creature.cpp \
	../src/island.cpp \
	../src/solver.cpp \
	../src/colour.cpp \
	../src/path.cpp \
	../src/action.cpp \
	main.cpp \
	pathchecker.cpp

INCLUDEPATH += ../src
