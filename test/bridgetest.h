#ifndef TST_BRIDGETEST_H
#define TST_BRIDGETEST_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include <vector>
#include <optional>
#include <memory>

#include "colour.h"
#include "connection.h"
#include "island.h"
#include "creature.h"
#include "solver.h"
#include "path.h"
#include "action.h"
#include "pathchecker.h"

using namespace testing;

class BridgeTest: public ::testing::Test
{
protected:
    BridgeTest()
    {
        //Setup islands
        endIsland->destinations = 1;

        //Setup creature
        startIsland->creatures.push_back(creature);

        //Setup connections
        bridge->updateIslands();

        //Store in world
        world.islands.push_back(startIsland);
        world.islands.push_back(endIsland);
        world.connections.push_back(bridge);
    }

    void SetUp() override { }

    void TearDown() override { }

    ~BridgeTest() override { }

    Solver world;
    const std::shared_ptr<Island> startIsland = std::make_shared<Island>();
    const std::shared_ptr<Island> endIsland = std::make_shared<Island>();
    const std::shared_ptr<Bridge> bridge =
            std::make_shared<Bridge>(startIsland, endIsland, Colour::Invalid);
    const std::shared_ptr<Creature> creature = std::make_shared<Creature>(Colour::Invalid, startIsland);
};

TEST_F(BridgeTest, uncolouredCreatureCannotCrossBridge)
{
    creature->colour = Colour::Uncoloured;
    for (const auto colour: ColourHelper::Colours)
    {
        //Set bridge colour
        bridge->colour = colour;

        //Run solver
        const std::optional<Path> result = world.run();
        EXPECT_FALSE(result) << "Bridge colour is " << ColourHelper::toString(colour);
    }
}

TEST_F(BridgeTest, creatureCanCrossSameColourBridge)
{
    for (auto it = ColourHelper::baseBegin, end = ColourHelper::compositeEnd; it != end; ++it)
    {
        //Set creature colour
        creature->colour = *it;
        //Set bridge colour
        bridge->colour = *it;

        //Run solver
        const std::optional<Path> result = world.run();
        const std::string message = "Colour is " + ColourHelper::toString(*it);
        EXPECT_TRUE(result) << message;
        if (result)
            PathChecker::checkPath(result.value(), {ActionType::Connection}, message);
    }
}

TEST_F(BridgeTest, baseCreatureCannotCrossDifferentBaseBridge)
{
    for (auto creatureIt = ColourHelper::baseBegin, creatureEnd = ColourHelper::baseEnd;
         creatureIt != creatureEnd; ++creatureIt)
    {
        for (auto bridgeIt = ColourHelper::baseBegin, bridgeEnd = ColourHelper::baseEnd;
             bridgeIt != bridgeEnd; ++bridgeIt)
        {
            if (*creatureIt != *bridgeIt)
            {
                //Set creature colour
                creature->colour = *creatureIt;
                //Set bridge colour
                bridge->colour = *bridgeIt;

                //Run solver
                const std::optional<Path> result = world.run();
                EXPECT_FALSE(result) << "Creature colour is " <<
                                                    ColourHelper::toString(*creatureIt) <<
                                                    ", Bridge colour is " <<
                                                    ColourHelper::toString(*bridgeIt);
            }
        }
    }
}

TEST_F(BridgeTest, compositeCreatureCanCrossPartOfCompositeBaseBridge)
{
    for (auto it = ColourHelper::compositeBegin, end = ColourHelper::compositeEnd; it != end; ++it)
    {
        const auto creatureColourParts = ColourHelper::split(*it);
        const auto bridgeColours = {creatureColourParts->first, creatureColourParts->second};
        for (const Colour bridgeColour: bridgeColours)
        {
            //Set creature colour
            creature->colour = *it;
            //Set bridge colour
            bridge->colour = bridgeColour;

            //Run solver
            const std::optional<Path> result = world.run();
            const std::string message = "Creature colour is " + ColourHelper::toString(*it)
                    + ", Bridge colour is " + ColourHelper::toString(bridgeColour);
            EXPECT_TRUE(result) << message;
            if (result)
                PathChecker::checkPath(result.value(), {ActionType::Connection}, message);
        }
    }
}

TEST_F(BridgeTest, compositeCreatureCannotCrossNotPartOfCompositeBaseBridge)
{
    for (auto creatureIt = ColourHelper::compositeBegin, creatureEnd = ColourHelper::compositeEnd;
         creatureIt != creatureEnd; ++creatureIt)
    {
        const auto creatureColourParts = ColourHelper::split(*creatureIt);
        for (auto bridgeIt = ColourHelper::baseBegin, bridgeEnd = ColourHelper::baseEnd;
             bridgeIt != bridgeEnd; ++bridgeIt)
        {
            if (*bridgeIt != creatureColourParts->first && *bridgeIt != creatureColourParts->second)
            {
                //Set creature colour
                creature->colour = *creatureIt;
                //Set bridge colour
                bridge->colour = *bridgeIt;

                //Run solver
                const std::optional<Path> result = world.run();
                EXPECT_FALSE(result) << "Creature colour is " <<
                                                    ColourHelper::toString(*creatureIt) <<
                                                    ", Bridge colour is " <<
                                                    ColourHelper::toString(*bridgeIt);
            }
        }
    }
}

TEST_F(BridgeTest, baseCreatureCannotCrossCompositeBridge)
{
    for (auto creatureIt = ColourHelper::baseBegin, creatureEnd = ColourHelper::baseEnd;
         creatureIt != creatureEnd; ++creatureIt)
    {
        for (auto bridgeIt = ColourHelper::compositeBegin, bridgeEnd = ColourHelper::compositeEnd;
             bridgeIt != bridgeEnd; ++bridgeIt)
        {
            //Set creature colour
            creature->colour = *creatureIt;
            //Set bridge colour
            bridge->colour = *bridgeIt;

            //Run solver
            const std::optional<Path> result = world.run();
            EXPECT_FALSE(result) << "Creature colour is " << ColourHelper::toString(*creatureIt)
                                       << ", Bridge colour is " << ColourHelper::toString(*bridgeIt);
        }
    }
}

TEST_F(BridgeTest, compositeCreatureCannotCrossDifferentCompositeBridge)
{
    for (auto creatureIt = ColourHelper::compositeBegin, creatureEnd = ColourHelper::compositeEnd;
         creatureIt != creatureEnd; ++creatureIt)
    {
        for (auto bridgeIt = ColourHelper::compositeBegin, bridgeEnd = ColourHelper::compositeEnd;
             bridgeIt != bridgeEnd; ++bridgeIt)
        {
            if (*creatureIt != *bridgeIt)
            {
                //Set creature colour
                creature->colour = *creatureIt;
                //Set bridge colour
                bridge->colour = *bridgeIt;

                //Run solver
                const std::optional<Path> result = world.run();
                EXPECT_FALSE(result) << "Creature colour is " << ColourHelper::toString(*creatureIt)
                                           << ", Bridge colour is " << ColourHelper::toString(*bridgeIt);
            }
        }
    }
}

#endif // TST_BRIDGETEST_H
