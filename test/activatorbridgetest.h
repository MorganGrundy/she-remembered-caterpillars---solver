#ifndef ACTIVATORBRIDGETEST_H
#define ACTIVATORBRIDGETEST_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include <vector>
#include <optional>
#include <algorithm>

#include "colour.h"
#include "connection.h"
#include "island.h"
#include "creature.h"
#include "solver.h"
#include "path.h"
#include "action.h"
#include "pathchecker.h"

using namespace testing;

class ActivatorBridgeTest: public ::testing::Test
{
protected:
    ActivatorBridgeTest()
    {
        //Setup islands
        endIsland->destinations = 1;

        //Setup creature
        startIsland->creatures.push_back(creature);

        //Setup activator-bridge
        activatorBridge->updateIslands();

        //Store in world
        world.islands.push_back(startIsland);
        world.islands.push_back(endIsland);
        world.connections.push_back(activatorBridge);
    }

    void SetUp() override { }

    void TearDown() override { }

    ~ActivatorBridgeTest() override { }

    Solver world;
    const std::shared_ptr<Island> startIsland = std::make_shared<Island>();
    const std::shared_ptr<Island> endIsland = std::make_shared<Island>();
    const std::shared_ptr<ActivatorBridge> activatorBridge =
            std::make_shared<ActivatorBridge>(startIsland, endIsland);
    const std::shared_ptr<Creature> creature =
            std::make_shared<Creature>(Colour::Invalid, startIsland);
};

TEST_F(ActivatorBridgeTest, creatureCannotCrossUnactiveActivatorBridge)
{
    //Create new unreachable island and place activator on it
    const auto otherIsland = std::make_shared<Island>();
    world.islands.push_back(otherIsland);
    otherIsland->activators.push_back(activatorBridge);
    activatorBridge->activators.emplace(otherIsland, 1);
    for (auto it = ColourHelper::Colours.cbegin(), end = ColourHelper::Colours.cend(); it != end; ++it)
    {
        //Set creature colour
        creature->colour = *it;

        //Run solver
        const std::optional<Path> result = world.run();
        EXPECT_FALSE(result) << "Creature colour is " << ColourHelper::toString(*it);
    }
    //Remove island and activator
    activatorBridge->activators.clear();
    world.islands.erase(world.islands.cend());
}

TEST_F(ActivatorBridgeTest, creatureOnActivatorCannotCrossActivatorBridge)
{
    //Put activator on same island as creature
    startIsland->activators.push_back(activatorBridge);
    activatorBridge->activators.emplace(startIsland, 1);
    for (auto it = ColourHelper::Colours.cbegin(), end = ColourHelper::Colours.cend(); it != end; ++it)
    {
        if (it == ColourHelper::compositeBegin)
            endIsland->destinations = 2;

        //Set creature colour
        creature->colour = *it;

        //Run solver
        const std::optional<Path> result = world.run();
        EXPECT_FALSE(result) << "Creature colour is " << ColourHelper::toString(*it);
    }
    endIsland->destinations = 1;
}

TEST_F(ActivatorBridgeTest, creatureNotOnActivatorCanCrossActiveActivatorBridge)
{
    //Create new unreachable island and place activator on it
    const auto otherIsland = std::make_shared<Island>();
    world.islands.push_back(otherIsland);
    otherIsland->activators.push_back(activatorBridge);
    activatorBridge->activators.emplace(otherIsland, 1);
    //Create new creature and place on island
    const auto otherCreature = std::make_shared<Creature>(Colour::Invalid, otherIsland);
    otherIsland->creatures.push_back(otherCreature);
    for (auto it = ColourHelper::Colours.cbegin(), end = ColourHelper::Colours.cend(); it != end; ++it)
    {
        //Set creature colour
        creature->colour = *it;

        //Run solver
        const std::optional<Path> result = world.run();
        const std::string message = "Creature colour is " + ColourHelper::toString(*it);
        EXPECT_TRUE(result) << message;
        if (result)
            PathChecker::checkPath(result.value(), {ActionType::Connection}, message);
    }
    //Remove island, activator, and creature
    activatorBridge->activators.clear();
    otherIsland->creatures.clear();
    world.islands.erase(world.islands.cend());
}

TEST_F(ActivatorBridgeTest, multipleActivatorAndCreatureOnSingleIsland)
{
    //Create new unreachable island and place activator on it
    const auto otherIsland = std::make_shared<Island>();
    world.islands.push_back(otherIsland);
    otherIsland->activators.push_back(activatorBridge);
    activatorBridge->activators.emplace(otherIsland, 2);
    //Create new creature and place on island
    const auto creature1 = std::make_shared<Creature>(Colour::Invalid, otherIsland);
    otherIsland->creatures.push_back(creature1);
    const auto creature2 = std::make_shared<Creature>(Colour::Invalid, otherIsland);
    otherIsland->creatures.push_back(creature2);
    for (auto it = ColourHelper::Colours.cbegin(), end = ColourHelper::Colours.cend(); it != end;
         ++it)
    {
        //Set creature colour
        creature->colour = *it;

        //Run solver
        const std::optional<Path> result = world.run();
        const std::string message = "Creature colour is " + ColourHelper::toString(*it);
        EXPECT_TRUE(result) << message;
        if (result)
            PathChecker::checkPath(result.value(), {ActionType::Connection}, message);
    }
    //Remove island, activator, and creature
    activatorBridge->activators.clear();
    otherIsland->creatures.clear();
    world.islands.erase(world.islands.cend());
}

TEST_F(ActivatorBridgeTest, multipleActivatorSingleCreatureOnSingleIsland)
{
    //Create new unreachable island and place activator on it
    const auto otherIsland = std::make_shared<Island>();
    world.islands.push_back(otherIsland);
    otherIsland->activators.push_back(activatorBridge);
    activatorBridge->activators.emplace(otherIsland, 2);
    //Create new creature and place on island
    const auto creature1 = std::make_shared<Creature>(Colour::Invalid,
                                                                         otherIsland);
    otherIsland->creatures.push_back(creature1);
    for (auto it = ColourHelper::Colours.cbegin(), end = ColourHelper::Colours.cend(); it != end; ++it)
    {
        //Set creature colour
        creature->colour = *it;

        //Run solver
        const std::optional<Path> result = world.run();
        EXPECT_FALSE(result) << "Creature colour is " << ColourHelper::toString(*it);
    }
    //Remove island, activator, and creature
    activatorBridge->activators.clear();
    otherIsland->creatures.clear();
    world.islands.erase(world.islands.cend());
}

#endif // ACTIVATORBRIDGETEST_H
