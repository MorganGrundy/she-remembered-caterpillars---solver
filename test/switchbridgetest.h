#ifndef SWITCHBRIDGETEST_H
#define SWITCHBRIDGETEST_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include <vector>
#include <optional>

#include "colour.h"
#include "connection.h"
#include "island.h"
#include "creature.h"
#include "solver.h"
#include "path.h"
#include "action.h"
#include "pathchecker.h"

using namespace testing;

class SwitchBridgeTest: public ::testing::Test
{
protected:
    SwitchBridgeTest()
    {
        //Setup islands
        endIsland->destinations = 1;

        //Setup creature
        startIsland->creatures.push_back(creature);

        //Setup connections
        switchBridge->updateIslands();

        //Store in world
        world.islands.push_back(startIsland);
        world.islands.push_back(endIsland);
        world.connections.push_back(switchBridge);
    }

    void SetUp() override { }

    void TearDown() override { }

    ~SwitchBridgeTest() override { }

    Solver world;
    const std::shared_ptr<Island> startIsland = std::make_shared<Island>();
    const std::shared_ptr<Island> endIsland = std::make_shared<Island>();
    const std::shared_ptr<SwitchBridge> switchBridge =
            std::make_shared<SwitchBridge>(startIsland, endIsland);
    const std::shared_ptr<Creature> creature =
            std::make_shared<Creature>(Colour::Invalid, startIsland);
};

TEST_F(SwitchBridgeTest, uncolouredCreatureCanCrossSwitchBridge)
{
    //Set creature colour
    creature->colour = Colour::Uncoloured;

    //Run solver
    const std::optional<Path> result = world.run();
    const std::string message = "Creature is uncoloured";
    EXPECT_TRUE(result) << message;
    if (result)
        PathChecker::checkPath(result.value(), {ActionType::Connection}, message);
}

TEST_F(SwitchBridgeTest, baseCreatureCanCrossSwitchBridge)
{
    for (auto it = ColourHelper::baseBegin, end = ColourHelper::baseEnd; it != end; ++it)
    {
        //Set creature colour
        creature->colour = *it;

        //Run solver
        const std::optional<Path> result = world.run();
        const std::string message = "Creature colour is " + ColourHelper::toString(*it);
        EXPECT_TRUE(result) << message;
        if (result)
            PathChecker::checkPath(result.value(), {ActionType::Connection}, message);
    }
}

TEST_F(SwitchBridgeTest, compositeCreatureCannotCrossSwitchBridge)
{
    endIsland->destinations = 2;
    for (auto it = ColourHelper::compositeBegin, end = ColourHelper::compositeEnd; it != end; ++it)
    {
        //Set creature colour
        creature->colour = *it;

        //Run solver
        const std::optional<Path> result = world.run();
        EXPECT_FALSE(result) << "Creature colour is " << ColourHelper::toString(*it);
    }
    endIsland->destinations = 1;
}

TEST_F(SwitchBridgeTest, creatureCannotCrossSwitchBridgeFromWrongDirection)
{
    //Switch state of switch-bridge
    switchBridge->p_island1 = endIsland;
    switchBridge->p_island2 = startIsland;
    for (auto it = ColourHelper::Colours.cbegin(), end = ColourHelper::Colours.cend(); it != end;
         ++it)
    {
        //Set creature colour
        creature->colour = *it;

        //Run solver
        const std::optional<Path> result = world.run();
        EXPECT_FALSE(result) << "Creature colour is " << ColourHelper::toString(*it);
    }
    switchBridge->p_island1 = startIsland;
    switchBridge->p_island2 = endIsland;
}

TEST_F(SwitchBridgeTest, switchBridgeSwitchesWhenCrossed)
{
    //Create another island with destination connected to start island by bridge
    const auto otherIsland = std::make_shared<Island>();
    otherIsland->destinations = 1;
    const auto bridge = std::make_shared<Bridge>(startIsland, otherIsland, Colour::Red);
    bridge->updateIslands();
    world.islands.push_back(otherIsland);
    world.connections.push_back(bridge);
    //Create another creature on end island
    const auto otherCreature = std::make_shared<Creature>(Colour::Red, endIsland);
    creature->colour = Colour::Blue;
    otherIsland->creatures.push_back(otherCreature);

    //Run solver
    const std::optional<Path> result = world.run();
    const std::string message = "";
    EXPECT_TRUE(result) << message;
    if (result)
        PathChecker::checkPath(result.value(), {ActionType::Connection, ActionType::Connection,
                                                ActionType::Connection}, message);

    bridge->p_island1 = nullptr;
    startIsland->connections.erase(startIsland->connections.cend());
    world.connections.erase(world.connections.cend());
    world.islands.erase(world.islands.cend());
}

#endif // SWITCHBRIDGETEST_H
