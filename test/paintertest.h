#ifndef PAINTERTEST_H
#define PAINTERTEST_H

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include <vector>
#include <optional>
#include <algorithm>

#include "colour.h"
#include "connection.h"
#include "island.h"
#include "creature.h"
#include "solver.h"
#include "path.h"
#include "action.h"
#include "pathchecker.h"

using namespace testing;

class PainterTest: public ::testing::Test
{
protected:
    PainterTest()
    {
        //Setup islands
        endIsland->destinations = 1;

        //Setup creature
        startIsland->creatures.push_back(creature);

        //Store in world
        world.islands.push_back(startIsland);
        world.islands.push_back(endIsland);
    }

    void SetUp() override { }

    void TearDown() override { }

    ~PainterTest() override { }

    Solver world;
    const std::shared_ptr<Island> startIsland = std::make_shared<Island>();
    const std::shared_ptr<Island> endIsland = std::make_shared<Island>();
    const std::shared_ptr<Creature> creature =
            std::make_shared<Creature>(Colour::Invalid, startIsland);
};

TEST_F(PainterTest, uncolouredCreatureUsePainter)
{
    creature->colour = Colour::Uncoloured;
    //Create bridge (uncoloured creature cannot cross bridges)
    const auto bridge = std::make_shared<Bridge>(startIsland, endIsland, Colour::Invalid);
    bridge->updateIslands();
    world.connections.push_back(bridge);
    for (auto it = ColourHelper::baseBegin, end = ColourHelper::baseEnd; it != end; ++it)
    {
        bridge->colour = *it;
        startIsland->setPainter(*it, true);

        //Run solver
        const std::optional<Path> result = world.run();
        const std::string message = "Colour is " + ColourHelper::toString(*it);
        EXPECT_TRUE(result) << message;
        if (result)
            PathChecker::checkPath(result.value(), {ActionType::Painter, ActionType::Connection},
                                   message);

        startIsland->setPainter(*it, false);
    }
    //Remove bridge
    startIsland->connections.clear();
    endIsland->connections.clear();
    world.connections.erase(world.connections.cend());
}

TEST_F(PainterTest, colouredCreatureUsePainter)
{
    //Create gate (uncoloured creature can cross gates)
    const auto gate = std::make_shared<Gate>(startIsland, endIsland, Colour::Invalid);
    gate->updateIslands();
    world.connections.push_back(gate);
    for (auto it = ColourHelper::baseBegin, end = ColourHelper::baseEnd; it != end; ++it)
    {
        creature->colour = *it;
        gate->colour = *it;
        startIsland->setPainter(*it, true);

        //Run solver
        const std::optional<Path> result = world.run();
        const std::string message = "Colour is " + ColourHelper::toString(*it);
        EXPECT_TRUE(result) << message;
        if (result)
            PathChecker::checkPath(result.value(), {ActionType::Painter, ActionType::Connection},
                                   message);

        startIsland->setPainter(*it, false);
    }
    //Remove gate
    startIsland->connections.clear();
    endIsland->connections.clear();
    world.connections.erase(world.connections.cend());
}

#endif // PAINTERTEST_H
