
#include <gtest/gtest.h>
#include <vector>
#include <string>

#include "pathchecker.h"
#include "path.h"
#include "action.h"

PathChecker::PathChecker() {}

void PathChecker::checkPath(const Path &path, const std::vector<ActionType> &expected,
                            const std::string &message)
{
    EXPECT_EQ(path.actions.size(), expected.size()) << message;
    auto expectedIt = expected.cbegin(), expectedEnd = expected.cend();
    for (auto pathIt = path.actions.cbegin(), pathEnd = path.actions.cend();
         pathIt != pathEnd && expectedIt != expectedEnd; ++pathIt, ++expectedIt)
    {
        EXPECT_EQ(pathIt->getType(), *expectedIt) << message;
    }
}
