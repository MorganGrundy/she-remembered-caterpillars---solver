#ifndef PATHCHECKER_H
#define PATHCHECKER_H

#include <vector>
#include <string>

class Path;
class Action;
enum class ActionType;

class PathChecker
{
public:
    static void checkPath(const Path &path, const std::vector<ActionType> &expected,
                          const std::string &message);
private:
    PathChecker();
};

#endif // PATHCHECKER_H
