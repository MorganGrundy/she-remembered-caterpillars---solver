#include "bridgetest.h"
#include "gatetest.h"
#include "switchbridgetest.h"
#include "activatorbridgetest.h"
#include "creaturetest.h"
#include "paintertest.h"

#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
