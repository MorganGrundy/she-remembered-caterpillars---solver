QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase c++17
CONFIG -= app_bundle

TEMPLATE = app

HEADERS += \
		../src/connection.h \
		../src/creature.h \
		../src/island.h \
		../src/solver.h \
		../src/colour.h \
		../src/path.h \
		../src/action.h \
		../src/hexagonalgridlogic.h \
		../src/hexagonalcell.h \
		../src/hexagonalcellconnection.h \
		../src/hexagonaldirection.h

SOURCES +=  \
	../src/connection.cpp \
	../src/creature.cpp \
	../src/island.cpp \
	../src/solver.cpp \
	../src/colour.cpp \
	../src/path.cpp \
	../src/action.cpp \
	../src/hexagonalgridlogic.cpp \
	../src/hexagonalcell.cpp \
	../src/hexagonalcellconnection.cpp \
	solverbenchmark.cpp

INCLUDEPATH += ../src

DISTFILES += \
	levels/2-011.dat \
	levels/2-011.jpg \
	levels/3-013.dat \
	levels/3-013.jpg \
	levels/4-017.dat \
	levels/4-017.jpg \
	levels/4-020.dat \
	levels/4-020.jpg \
	levels/5-022.dat \
	levels/5-022.jpg \
	levels/5-026.dat \
	levels/5-026.jpg \
	levels/6-029.dat \
	levels/6-029.jpg \
	levels/6-033.dat \
	levels/6-033.jpg \
	levels/level2_011.dat
