#include <QtTest>
#include <optional>
#include <memory>

#include "hexagonalgridlogic.h"
#include "solver.h"
#include "island.h"
#include "colour.h"
#include "creature.h"
#include "connectiontype.h"
#include "connection.h"
#include "path.h"
#include "action.h"

class SolverBenchmark : public QObject
{
    Q_OBJECT
private:
    void checkPath(const Path &path, const std::vector<ActionType> &expected);
    std::optional<Solver> loadFromFile(const QString &filename);

    const QString worldSaves = "E:/Desktop/SheRememberedCaterpillars/SheRememberedCaterpillars-Solver/benchmark/levels/";
private slots:
    void simple();
    void level2_011();
    void level3_013();
    void level4_017();
    void level4_020();
    void level5_022();
    void level5_026();
    void level6_029();
    void level6_033();

};

void SolverBenchmark::checkPath(const Path &path, const std::vector<ActionType> &expected)
{
    QVERIFY2(path.actions.size() == expected.size(), path.toString().c_str());
    auto expectedIt = expected.cbegin(), expectedEnd = expected.cend();
    for (auto pathIt = path.actions.cbegin(), pathEnd = path.actions.cend();
         pathIt != pathEnd && expectedIt != expectedEnd; ++pathIt, ++expectedIt)
    {
        QVERIFY2(pathIt->getType() == *expectedIt, path.toString(expected).c_str());
    }
}

std::optional<Solver> SolverBenchmark::loadFromFile(const QString &filename)
{
    HexagonalGridLogic grid;
    const QString error = grid.loadFromFile(filename);
    if (!error.isEmpty())
        return std::nullopt;

    return grid.convertToSolver();
}

void SolverBenchmark::simple()
{
    const std::vector<ActionType> expectedSolution =
    {ActionType::Painter, ActionType::Connection, ActionType::Connection, ActionType::Connection};

    //Create world
    Solver world;
    for (unsigned int i = 0; i < 4; ++i)
        world.islands.push_back(std::make_shared<Island>());

    //Set start island with red painter and uncoloured creature
    world.islands.at(0)->setPainter(Colour::Red, true);
    world.islands.at(0)->creatures.push_back(
                std::make_shared<Creature>(Colour::Uncoloured, world.islands.at(0)));

    world.islands.at(3)->destinations = 1;

    //Create connection between islands
    for (unsigned int i = 0; i < 3; ++i)
    {
        world.connections.push_back(
                    std::make_shared<Bridge>(world.islands.at(i), world.islands.at(i+1),
                                             Colour::Red));
        world.connections.at(i)->updateIslands();
    }

    //Solve world, test result, and benchmark
    std::optional<Path> result = world.run(expectedSolution.size());
    QVERIFY(result);
    if (result)
        checkPath(result.value(), expectedSolution);
    QBENCHMARK {result = world.run();}
}

void SolverBenchmark::level2_011()
{
    const std::vector<ActionType> expectedSolution =
    {ActionType::Join, ActionType::Connection, ActionType::Connection, ActionType::Split,
     ActionType::Connection, ActionType::Join, ActionType::Connection, ActionType::Connection,
     ActionType::Connection, ActionType::Split, ActionType::Connection, ActionType::Connection,
     ActionType::Connection, ActionType::Connection, ActionType::Connection};

    //Attempt to load world file for level2-011
    const QString worldFile = worldSaves + "2-011.dat";
    std::optional<Solver> world = loadFromFile(worldFile);
    QVERIFY(world);

    //Solve world, test result, and benchmark
    std::optional<Path> result = world->run(expectedSolution.size());
    QVERIFY(result);
    if (result)
        checkPath(result.value(), expectedSolution);
    QBENCHMARK {result = world->run();}
}

void SolverBenchmark::level3_013()
{
    const std::vector<ActionType> expectedSolution =
    {ActionType::Connection, ActionType::Connection, ActionType::Join, ActionType::Connection,
     ActionType::Split, ActionType::Connection, ActionType::Join, ActionType::Connection,
     ActionType::Connection, ActionType::Split, ActionType::Connection, ActionType::Join,
     ActionType::Connection, ActionType::Split, ActionType::Join, ActionType::Connection,
     ActionType::Split, ActionType::Connection, ActionType::Join,  ActionType::Connection,
     ActionType::Connection, ActionType::Split, ActionType::Connection, ActionType::Connection,
     ActionType::Connection};

    //Attempt to load world file for level3-013
    const QString worldFile = worldSaves + "3-013.dat";
    std::optional<Solver> world = loadFromFile(worldFile);
    QVERIFY(world);

    //Solve world, test result, and benchmark
    std::optional<Path> result = world->run(expectedSolution.size());
    QVERIFY(result);
    if (result)
        checkPath(result.value(), expectedSolution);
    QBENCHMARK {result = world->run();}
}

void SolverBenchmark::level4_017()
{
    const std::vector<ActionType> expectedSolution =
    {ActionType::Connection, ActionType::Connection, ActionType::Connection, ActionType::Join,
     ActionType::Connection, ActionType::Split, ActionType::Connection, ActionType::Connection,
     ActionType::Join, ActionType::Connection, ActionType::Split, ActionType::Connection,
     ActionType::Join, ActionType::Connection, ActionType::Split, ActionType::Connection,
     ActionType::Connection, ActionType::Connection, ActionType::Join, ActionType::Connection};

    //Attempt to load world file for level4-017
    const QString worldFile = worldSaves + "4-017.dat";
    std::optional<Solver> world = loadFromFile(worldFile);
    QVERIFY(world);

    //Solve world, test result, and benchmark
    std::optional<Path> result = world->run(expectedSolution.size());
    QVERIFY(result);
    if (result)
        checkPath(result.value(), expectedSolution);
    QBENCHMARK {result = world->run();}
}

void SolverBenchmark::level4_020()
{
    const std::vector<ActionType> expectedSolution =
    {ActionType::Connection, ActionType::Connection, ActionType::Connection, ActionType::Connection,
     ActionType::Connection, ActionType::Split, ActionType::Connection, ActionType::Connection,
     ActionType::Connection, ActionType::Join, ActionType::Connection, ActionType::Connection,
     ActionType::Split, ActionType::Connection, ActionType::Join, ActionType::Connection,
     ActionType::Split, ActionType::Connection, ActionType::Connection};

    //Attempt to load world file for level4-020
    const QString worldFile = worldSaves + "4-020.dat";
    std::optional<Solver> world = loadFromFile(worldFile);
    QVERIFY(world);

    //Solve world, test result, and benchmark
    std::optional<Path> result = world->run(expectedSolution.size());
    QVERIFY(result);
    if (result)
        checkPath(result.value(), expectedSolution);
    QBENCHMARK {result = world->run();}
}

void SolverBenchmark::level5_022()
{
    const std::vector<ActionType> expectedSolution =
    {ActionType::Painter, ActionType::Connection, ActionType::Connection, ActionType::Connection,
     ActionType::Painter, ActionType::Connection, ActionType::Join, ActionType::Connection,
     ActionType::Split, ActionType::Connection, ActionType::Connection, ActionType::Connection,
     ActionType::Connection, ActionType::Connection, ActionType::Connection, ActionType::Painter,
     ActionType::Connection, ActionType::Connection, ActionType::Connection, ActionType::Painter,
     ActionType::Connection, ActionType::Join, ActionType::Connection, ActionType::Split,
     ActionType::Connection, ActionType::Connection, ActionType::Connection, ActionType::Connection,
     ActionType::Join, ActionType::Connection, ActionType::Split, ActionType::Connection,
     ActionType::Connection, ActionType::Connection, ActionType::Connection, ActionType::Connection,
     ActionType::Connection, ActionType::Painter, ActionType::Connection};

    //Attempt to load world file for level5-022
    const QString worldFile = worldSaves + "5-022.dat";
    std::optional<Solver> world = loadFromFile(worldFile);
    QVERIFY(world);

    //Solve world, test result, and benchmark
    std::optional<Path> result = world->run(expectedSolution.size());
    QVERIFY(result);
    if (result)
        checkPath(result.value(), expectedSolution);
    QBENCHMARK {result = world->run();}
}

void SolverBenchmark::level5_026()
{
    const std::vector<ActionType> expectedSolution =
    {ActionType::Connection, ActionType::Connection, ActionType::Split, ActionType::Connection,
     ActionType::Join, ActionType::Connection, ActionType::Split, ActionType::Join,
     ActionType::Connection, ActionType::Split, ActionType::Connection, ActionType::Join,
     ActionType::Connection, ActionType::Split, ActionType::Connection, ActionType::Connection,
     ActionType::Painter, ActionType::Connection, ActionType::Painter, ActionType::Connection,
     ActionType::Join, ActionType::Connection, ActionType::Split, ActionType::Connection,
     ActionType::Connection, ActionType::Connection, ActionType::Painter, ActionType::Connection,
     ActionType::Connection};

    //Attempt to load world file for level5-026
    const QString worldFile = worldSaves + "5-026.dat";
    std::optional<Solver> world = loadFromFile(worldFile);
    QVERIFY(world);

    //Solve world, test result, and benchmark
    std::optional<Path> result = world->run(expectedSolution.size());
    QVERIFY(result);
    if (result)
        checkPath(result.value(), expectedSolution);
    QBENCHMARK {result = world->run();}
}

void SolverBenchmark::level6_029()
{
    const std::vector<ActionType> expectedSolution =
    {ActionType::Split, ActionType::Connection, ActionType::Connection, ActionType::Connection,
     ActionType::Connection, ActionType::Connection, ActionType::Connection, ActionType::Connection,
     ActionType::Join, ActionType::Connection};

    //Attempt to load world file for level6-029
    const QString worldFile = worldSaves + "6-029.dat";
    std::optional<Solver> world = loadFromFile(worldFile);
    QVERIFY(world);

    //Solve world, test result, and benchmark
    std::optional<Path> result = world->run(expectedSolution.size());
    QVERIFY(result);
    if (result)
        checkPath(result.value(), expectedSolution);
    QBENCHMARK {result = world->run();}
}

void SolverBenchmark::level6_033()
{
    const std::vector<ActionType> expectedSolution =
    {ActionType::Connection, ActionType::Connection, ActionType::Connection, ActionType::Connection,
     ActionType::Split, ActionType::Connection, ActionType::Join, ActionType::Connection,
     ActionType::Split, ActionType::Connection, ActionType::Connection, ActionType::Join,
     ActionType::Connection, ActionType::Split, ActionType::Connection, ActionType::Connection,
     ActionType::Connection, ActionType::Connection, ActionType::Connection};

    //Attempt to load world file for level6-033
    const QString worldFile = worldSaves + "6-033.dat";
    std::optional<Solver> world = loadFromFile(worldFile);
    QVERIFY(world);

    //Solve world, test result, and benchmark
    std::optional<Path> result = world->run(expectedSolution.size());
    QVERIFY(result);
    if (result)
        checkPath(result.value(), expectedSolution);
    QBENCHMARK {result = world->run();}
}

QTEST_MAIN(SolverBenchmark)
#include "solverbenchmark.moc"
