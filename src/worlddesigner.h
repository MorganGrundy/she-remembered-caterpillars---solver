#ifndef WORLDDESIGNER_H
#define WORLDDESIGNER_H

#include <memory>
#include <QMainWindow>
#include <QAbstractButton>
#include <QMenu>

namespace Ui {
class WorldDesigner;
}

class WorldDesigner : public QMainWindow
{
    Q_OBJECT

public:
    explicit WorldDesigner(std::shared_ptr<QWidget> parent = nullptr);
    ~WorldDesigner();

private:
    std::shared_ptr<Ui::WorldDesigner> m_ui;
    std::shared_ptr<QMenu> m_connectionMenu;

private slots:
    void modeButtonReleased(QAbstractButton *button);
    void colourSelectorChanged(const QString &text);
    void toolBarTriggered(QAction *action);
};

#endif // WORLDDESIGNER_H
