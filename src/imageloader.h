#ifndef IMAGELOADER_H
#define IMAGELOADER_H

#include <QString>

#include "colour.h"

class ImageLoader
{
public:
    static QString loadCreature(Colour t_colour);
    static QString loadPainter(Colour t_colour);
    static QString loadBridge(Colour t_colour);
    static QString loadGate(Colour t_colour);
    static QString loadActivatorBridge(bool t_active);

    static const QString switchBridgePath;
    static const QString activatorPath;
    static const QString destinationPath;

private:
    static const QString creaturePath;
    static const QString painterPath;
    static const QString bridgePath;
    static const QString gatePath;
    static const QString activatorBridgePath;
};

#endif // IMAGELOADER_H
