#ifndef HEXAGONALCELLCONNECTION_H
#define HEXAGONALCELLCONNECTION_H

#include <vector>
#include <memory>

#include "colour.h"
#include "connectiontype.h"
#include "hexagonaldirection.h"

class HexagonalCell;

class HexagonalCellConnection : public std::enable_shared_from_this<HexagonalCellConnection>
{
public:
    HexagonalCellConnection(ConnectionType t_type, Colour t_colour,
                            std::vector<std::shared_ptr<HexagonalCell>> t_cells);
    void initialise();

    HexagonalDirection getDirection() const;

    size_t getCellSize() const;
    std::shared_ptr<HexagonalCell> getCellAt(size_t t_index);
    void switchDirection();

    void remove();

    const ConnectionType m_type;
    const Colour m_colour;

private:
    std::vector<std::shared_ptr<HexagonalCell>> m_cells;
    HexagonalDirection m_direction;
};

#endif // HEXAGONALCELLCONNECTION_H
