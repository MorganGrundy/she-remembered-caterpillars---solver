#include "solver.h"

#include <optional>
#include <memory>
#include <vector>
#include <algorithm>
#include <QString>
#include <QDebug>

#include "colour.h"
#include "connection.h"
#include "creature.h"
#include "island.h"
#include "path.h"

Solver::Solver() {}

void Solver::addPath(const Path &t_path)
{
    if (SolverState newState(t_path); states.find(newState) == states.cend())
    {
        qDebug() << "Action accepted." << newState.id;
        paths.insert(t_path);
        states.insert(newState);
    }
    else
        qDebug() << "Action denied. Conflict with" << states.find(newState)->id;
}

//Creates and returns the initial path from a vector switch-bridges and activator-bridges
Path Solver::createInitialPath(const std::vector<std::shared_ptr<SwitchBridge>> &t_switchBridges)
const
{
    Path initial;
    //Initially all switch bridges go from island 1 to island 2
    initial.switchBridgeState = std::vector<bool>(t_switchBridges.size(), false);

    //Save creatures
    for (auto island: islands)
    {
        for (auto creature: island->creatures)
        {
            //If colour is a join then need to create two creatures
            if (creature->colour >= *ColourHelper::compositeBegin)
            {
                auto splitColours = ColourHelper::split(creature->colour);
                auto firstCreature = std::make_shared<Creature>(splitColours->first, island);
                auto secondCreature = std::make_shared<Creature>(splitColours->second, island);
                firstCreature->p_joinedCreature = secondCreature;
                firstCreature->b_main = true;
                firstCreature->joinedColour = creature->colour;
                secondCreature->p_joinedCreature = firstCreature;
                secondCreature->joinedColour = creature->colour;

                initial.creatures.push_back(firstCreature);
                initial.creatures.push_back(secondCreature);
            }
            else
                initial.creatures.push_back(creature);
        }
    }

    return initial;
}

//Return if the given path activates all destinations
bool Solver::pathIsValid(const Path &t_path,
                         const std::vector<std::shared_ptr<Island>> &t_destinations) const
{
    bool destinationsReached = true;
    //For all islands with destinations
    for (auto destination: t_destinations)
    {
        long long count = destination->destinations;
        for (auto creature: t_path.creatures)
        {
            if (creature->p_location == destination)
                --count;
        }
        destinationsReached &= (count <= 0);
    }

    return destinationsReached;
}

//Performs all possible join actions on current path and adds to paths
void Solver::joinActions(const Path &t_currentPath)
{
    for (auto it = t_currentPath.creatures.cbegin(), end = t_currentPath.creatures.cend();
         it != end; ++it)
    {
        if (!(*it)->p_joinedCreature && (*it)->colour != Colour::Uncoloured)
        {
            for (auto it2 = it+1; it2 != end; ++it2)
            {
                if ((*it)->canJoin(*it2))
                {
                    //Join action
                    Path newPath(t_currentPath);
                    //Get matching creatures in newPath
                    std::shared_ptr<Creature> creature1 = newPath.creatures.at(
                                static_cast<size_t>(std::distance(
                                                        t_currentPath.creatures.cbegin(), it)));

                    std::shared_ptr<Creature> creature2 = newPath.creatures.at(
                                static_cast<size_t>(std::distance(
                                                        t_currentPath.creatures.cbegin(), it2)));
                    newPath.actions.push_back(
                                Action(Action::JoinOrSplit::Join, creature1, creature2));
                    qDebug() << "New join action";
                    addPath(newPath);
                }
            }
        }
    }
}

//Performs all possible split actions on current path and adds to paths
void Solver::splitActions(const Path &t_currentPath)
{
    for (auto it = t_currentPath.creatures.cbegin(), end = t_currentPath.creatures.cend();
         it != end; ++it)
    {
        if ((*it)->canSplit())
        {
            //Split action
            Path newPath(t_currentPath);
            //Get matching creature in newPath
            std::shared_ptr<Creature> creature = newPath.creatures.at(
                        static_cast<size_t>(std::distance(t_currentPath.creatures.cbegin(), it)));
            newPath.actions.push_back(
                        Action(Action::JoinOrSplit::Split, creature, creature->p_joinedCreature));
            qDebug() << "New split action";
            addPath(newPath);
        }
    }
}

//Performs all possible painter actions on current path and adds to paths
void Solver::painterActions(const Path &t_currentPath)
{
    for (auto it = t_currentPath.creatures.cbegin(), end = t_currentPath.creatures.cend();
         it != end; ++it)
    {
        //Painter only use base colours, so no joined creatures
        if (!(*it)->p_joinedCreature)
        {
            //Uncoloured creature can use any painter
            if ((*it)->colour == Colour::Uncoloured)
            {
                for (auto colour = ColourHelper::baseBegin, end = ColourHelper::baseEnd;
                     colour != end; ++colour)
                {
                    if ((*it)->p_location->getPainter(*colour))
                    {
                        //Painter action
                        Path newPath(t_currentPath);
                        //Get matching creature in newPath
                        std::shared_ptr<Creature> creature = newPath.creatures.at(
                                    static_cast<size_t>(std::distance(
                                                            t_currentPath.creatures.cbegin(), it)));
                        newPath.actions.push_back(Action(creature, *colour));
                        qDebug() << "New painter action";
                        addPath(newPath);
                    }
                }
            }
            //Creature is a base colour, can only use same colour painter
            else
            {
                if ((*it)->p_location->getPainter((*it)->colour))
                {
                    //Painter action
                    Path newPath(t_currentPath);
                    //Get matching creatures in newPath
                    std::shared_ptr<Creature> creature = newPath.creatures.at(
                                static_cast<size_t>(std::distance(
                                                        t_currentPath.creatures.cbegin(), it)));
                    newPath.actions.push_back(Action(creature, (*it)->colour));
                    qDebug() << "New painter action";
                    addPath(newPath);
                }
            }
        }
    }
}

//Performs all possible connection actions on current path and adds to paths
Path Solver::createConnectionAction(const Path &t_currentPath, const size_t t_creature,
                                    const std::shared_ptr<Connection> t_connection) const
{
    Path newPath(t_currentPath);
    newPath.actions.push_back(Action(newPath.creatures.at(t_creature), t_connection));
    return newPath;
}

void Solver::connectionActions(const Path &t_currentPath,
                               const std::vector<std::shared_ptr<SwitchBridge>> &t_switchBridges)
{
    for (auto it = t_currentPath.creatures.cbegin(), end = t_currentPath.creatures.cend();
         it != end; ++it)
    {
        if (!(*it)->p_joinedCreature || (*it)->b_main)
        {
            for (auto connectionIt = (*it)->p_location->connections.cbegin(),
                 connectionEnd = (*it)->p_location->connections.cend();
                 connectionIt != connectionEnd; ++connectionIt)
            {
                if ((*connectionIt)->type == ConnectionType::SwitchBridge)
                {
                    //Only base or uncoloured can cross switch bridge, hence no joined creatures
                    if (!(*it)->p_joinedCreature)
                    {
                        //Find connection in list of switch bridge
                        auto switchBridge = std::find(t_switchBridges.cbegin(),
                                                      t_switchBridges.cend(), *connectionIt);
                        if (switchBridge != t_switchBridges.cend())
                        {
                            const size_t switchBridgeIndex = static_cast<size_t>(
                                        std::distance(t_switchBridges.cbegin(), switchBridge));

                            const bool switchBridgeState =
                                    t_currentPath.switchBridgeState.at(switchBridgeIndex);
                            //Check if creature and switch bridge in same direction
                            if ((*it)->p_location == (*switchBridge)->p_island1 &&
                                    !switchBridgeState)
                            {
                                //Connection action
                                const size_t creatureIndex = static_cast<size_t>(
                                            std::distance(t_currentPath.creatures.cbegin(), it));
                                Path newPath = createConnectionAction(t_currentPath, creatureIndex,
                                                                      *connectionIt);
                                newPath.switchBridgeState.at(switchBridgeIndex) = true;
                                qDebug() << "New switch-bridge action";
                                addPath(newPath);
                            }
                            else if ((*it)->p_location == (*switchBridge)->p_island2 &&
                                     switchBridgeState)
                            {
                                //Connection action
                                const size_t creatureIndex = static_cast<size_t>(
                                            std::distance(t_currentPath.creatures.cbegin(), it));
                                Path newPath = createConnectionAction(t_currentPath, creatureIndex,
                                                                      *connectionIt);
                                newPath.switchBridgeState.at(switchBridgeIndex) = false;
                                qDebug() << "New switch-bridge action";
                                addPath(newPath);
                            }
                        }
                    }
                }
                else if ((*connectionIt)->type == ConnectionType::ActivatorBridge)
                {
                    std::shared_ptr<ActivatorBridge> activatorBridge
                            = std::static_pointer_cast<ActivatorBridge>(*connectionIt);
                    bool canCross = true;
                    //Loop over all activators for current activator-bridge
                    for (auto activator: activatorBridge->activators)
                    {
                        int count = activator.second;
                        for (auto creature2: t_currentPath.creatures)
                        {
                            //If creature at activator and is not the crossing creature
                            if (creature2->p_location == activator.first
                                && creature2 != *it
                                && creature2->p_joinedCreature != *it)
                            {
                                --count;
                            }
                        }
                        //Not all activators on island are activated, hence cannot cross
                        if (count > 0)
                        {
                            canCross = false;
                            break;
                        }
                    }

                    if (canCross)
                    {
                        //Connection action
                        const size_t index = static_cast<size_t>(
                                    std::distance(t_currentPath.creatures.cbegin(), it));
                        qDebug() << "New activator-bridge action";
                        addPath(createConnectionAction(t_currentPath, index, (*connectionIt)));
                    }
                }
                else if ((*connectionIt)->type == ConnectionType::Bridge)
                {
                    //Connection action
                    if (std::reinterpret_pointer_cast<Bridge>(*connectionIt)->canCross((*it)))
                    {
                        const size_t index = static_cast<size_t>(
                                    std::distance(t_currentPath.creatures.cbegin(), it));
                        qDebug() << "New bridge action";
                        addPath(createConnectionAction(t_currentPath, index, (*connectionIt)));
                    }
                }
                else if ((*connectionIt)->type == ConnectionType::Gate)
                {
                    //Connection action
                    if (std::reinterpret_pointer_cast<Gate>(*connectionIt)->canCross((*it)))
                    {
                        const size_t index = static_cast<size_t>(
                                    std::distance(t_currentPath.creatures.cbegin(), it));
                        qDebug() << "New gate action";
                        addPath(createConnectionAction(t_currentPath, index, (*connectionIt)));
                    }
                }
            }
        }
    }
}

std::optional<Path> Solver::run(const size_t t_maxActions, const size_t t_maxPaths)
{
    //Create list of switch bridges and activator bridges
    std::vector<std::shared_ptr<SwitchBridge>> switchBridges;
    std::vector<std::shared_ptr<ActivatorBridge>> activatorBridges;
    for (auto connection: connections)
    {
        if (connection->type == ConnectionType::SwitchBridge)
            switchBridges.push_back(std::static_pointer_cast<SwitchBridge>(connection));
        else if (connection->type == ConnectionType::ActivatorBridge)
            activatorBridges.push_back(std::static_pointer_cast<ActivatorBridge>(connection));
    }

    //Ordered set of paths, ordered by size of path then most recent action
    paths.insert(createInitialPath(switchBridges));
    states.insert(SolverState(*paths.cbegin()));
    qDebug() << "Initial state:" << states.cbegin()->id;

    //Create list of islands with destinations
    std::vector<std::shared_ptr<Island>> destinations;
    for (auto island: islands)
        if (island->destinations > 0)
            destinations.push_back(island);

    std::optional<Path> solution;
    //While active paths exist and no solution found
    while (!paths.empty() && !solution)
    {
        if (debugMode)
            qInfo() << "Number of paths: " << paths.size();
        //Limits number of paths
        //if (paths.size() > t_maxPaths)
            //break;

        const auto pathIt = paths.begin();
        const Path currentPath = *pathIt;
        paths.erase(pathIt);
        //Limits number of actions in a path
        if (currentPath.actions.size() > t_maxActions)
        {
            qInfo() << "Max actions";
            break;
        }

        if (pathIsValid(currentPath, destinations))
        {
            solution = currentPath;
            break;
        }

        if (debugMode)
            qInfo() << "Current path: " << QString::fromStdString(currentPath.toString());

        joinActions(currentPath);
        splitActions(currentPath);
        painterActions(currentPath);
        connectionActions(currentPath, switchBridges);
    }

    if (debugMode)
    {
        if (paths.empty())
            qInfo() << "No paths left";

        if (solution)
            qInfo() << "Solution path: " << QString::fromStdString(solution->toString());
        else
            qInfo() << "No solution found :(";
    }

    states.clear();
    paths.clear();

    return solution;
}
