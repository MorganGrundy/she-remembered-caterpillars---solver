#ifndef ISLAND_H
#define ISLAND_H

#include <vector>
#include <memory>

#include "colour.h"
#include "creature.h"
#include "connection.h"

class Island : public std::enable_shared_from_this<Island>
{
public:
    Island();

    bool getPainter(const Colour t_colour) const;
    void setPainter(const Colour t_colour, const bool state);

    unsigned int destinations = 0; //Number of destinations on island

    bool b_redPainter = false; //If island has a red painter
    bool b_yellowPainter = false; //If island has a yellow painter
    bool b_bluePainter = false; //If island has a blue painter

    std::vector<std::shared_ptr<Connection>> connections; //Connections on island
    //Activator bridges that have activators on island
    std::vector<std::shared_ptr<ActivatorBridge>> activators;

    std::vector<std::shared_ptr<Creature>> creatures; //Creatures on this island

    size_t id;

private:
    static size_t nextID;
};

#endif // ISLAND_H
