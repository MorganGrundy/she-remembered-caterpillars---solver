#-------------------------------------------------
#
# Project created by QtCreator 2019-05-15T23:37:20
#
#-------------------------------------------------

QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SRBSolver
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++17

SOURCES += \
	action.cpp \
	colour.cpp \
	connection.cpp \
	creature.cpp \
	hexagonalcell.cpp \
	hexagonalcellconnection.cpp \
	hexagonalgrid.cpp \
	hexagonalgridlogic.cpp \
	imageloader.cpp \
	island.cpp \
	main.cpp \
	path.cpp \
	solver.cpp \
	worlddesigner.cpp

HEADERS += \
	action.h \
    colour.h \
	connection.h \
	connectiontype.h \
	creature.h \
	hexagonalcell.h \
	hexagonalcellconnection.h \
	hexagonaldirection.h \
	hexagonalgrid.h \
	hexagonalgridlogic.h \
	imageloader.h \
	island.h \
    path.h \
    solver.h \
	worlddesigner.h

FORMS += \
    worlddesigner.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
	Actions.txt \
	Connections.txt \
	Images/GUIDesign.png \
	solutions.txt

RESOURCES += \
	world.qrc
