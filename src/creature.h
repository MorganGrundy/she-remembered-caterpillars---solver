#ifndef CREATURE_H
#define CREATURE_H

#include <memory>

#include "colour.h"

class Island;

class Creature : public std::enable_shared_from_this<Creature>
{
public:
    Creature(const Colour t_colour, const std::shared_ptr<Island> t_location);

    bool canSplit() const;
    bool canJoin(const std::shared_ptr<Creature> t_creature) const;
    bool split();
    bool join(const std::shared_ptr<Creature> t_creature);

    Colour colour;
    std::shared_ptr<Island> p_location; //Pointer to island that the creature is on
    std::shared_ptr<Creature> p_joinedCreature;
    bool b_main;
    Colour joinedColour;

    size_t id;

private:
    static size_t nextID;
};

#endif // CREATURE_H
