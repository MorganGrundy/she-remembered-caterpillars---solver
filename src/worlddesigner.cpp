#include "worlddesigner.h"
#include "ui_worlddesigner.h"

#include <memory>
#include <QWidget>
#include <QAbstractButton>
#include <QMenu>
#include <QFileDialog>
#include <QMessageBox>

#include "colour.h"
#include "imageloader.h"

WorldDesigner::WorldDesigner(std::shared_ptr<QWidget> parent) :
    QMainWindow{parent.get()},
    m_ui{std::make_shared<Ui::WorldDesigner>()}
{
    m_ui->setupUi(this);

    //Create menu and add to connectionButton
    m_connectionMenu = std::make_shared<QMenu>(this);
    m_connectionMenu->setStyleSheet("QMenu{"
                                  "border: 2px solid #225;"
                                  "border-style: outset;"
                                  "padding: 10px;"
                                  "background: qradialgradient(cx: 0.3, cy: -0.4, fx: 0.3, "
                                  "fy: -0.4, radius: 1.35, stop: 0 #88F, stop: 1 #000);"
                                  "color: rgb(255, 255, 255);"
                                  "}"
                                  "QMenu::item:selected{"
                                  "border: 1px solid #AAF;"
                                  "}");
    m_ui->connectionButton->setMenu(m_connectionMenu.get());

    //Add bridge, gate, switch-bridge, activator-bridge, and activator to menu
    //When menu item clicked update button icon and text
    m_connectionMenu->addAction(QIcon(ImageLoader::loadBridge(Colour::Red)), "Bridge", this,
                                [this]() {
        m_ui->connectionButton->setIcon(QIcon(ImageLoader::loadBridge(m_ui->grid->currentColour)));
        m_ui->connectionButton->setText("Bridge");
        m_ui->connectionButton->click();
    });

    m_connectionMenu->addAction(QIcon(ImageLoader::loadGate(Colour::Red)), "Gate", this,
                                [this]() {
        m_ui->connectionButton->setIcon(QIcon(ImageLoader::loadGate(m_ui->grid->currentColour)));
        m_ui->connectionButton->setText("Gate");
        m_ui->connectionButton->click();
    });

    m_connectionMenu->addAction(QIcon(ImageLoader::switchBridgePath),"Switch-Bridge", this,
                                [this]() {
        m_ui->connectionButton->setIcon(QIcon(ImageLoader::switchBridgePath));
        m_ui->connectionButton->setText("Switch-Bridge");
        m_ui->connectionButton->click();
    });

    m_connectionMenu->addAction(QIcon(ImageLoader::loadActivatorBridge(true)), "Activator-Bridge", this,
                                [this]() {
        m_ui->connectionButton->setIcon(QIcon(ImageLoader::loadActivatorBridge(true)));
        m_ui->connectionButton->setText("Activator-Bridge");
        m_ui->connectionButton->click();
    });

    m_connectionMenu->addAction(QIcon(ImageLoader::activatorPath), "Activator", this,
                                [this]() {
        m_ui->connectionButton->setIcon(QIcon(ImageLoader::activatorPath));
        m_ui->connectionButton->setText("Activator");
        m_ui->connectionButton->click();
    });

    connect(m_ui->modeButtons, SIGNAL(buttonReleased(QAbstractButton *)), this,
            SLOT(modeButtonReleased(QAbstractButton *)));
    connect(m_ui->colourSelector, SIGNAL(currentIndexChanged(const QString &)), this,
            SLOT(colourSelectorChanged(const QString &)));
    connect(m_ui->toolBar, SIGNAL(actionTriggered(QAction *)), this,
            SLOT(toolBarTriggered(QAction *)));

    m_ui->islandButton->click();
}

//Called when any of the mode buttons is clicked
//Changes the mode and updates the colours in colourSelector
void WorldDesigner::modeButtonReleased(QAbstractButton *button)
{
    const QString &previousColour = m_ui->colourSelector->currentText();

    if (button->text().compare("Island") == 0)
    {
        m_ui->grid->currentMode = HexagonalGrid::Mode::Island;
        m_ui->colourSelector->setEnabled(false);
    }
    else if (button->text().compare("Creature") == 0)
    {
        m_ui->grid->currentMode = HexagonalGrid::Mode::Creature;
        m_ui->colourSelector->setEnabled(true);
        m_ui->colourSelector->clear();
        for (const auto col: ColourHelper::Colours)
            m_ui->colourSelector->addItem(QString::fromStdString(ColourHelper::toString(col)));
    }
    else if (button->text().compare("Bridge") == 0)
    {
        m_ui->grid->currentMode = HexagonalGrid::Mode::Bridge;
        m_ui->colourSelector->setEnabled(true);
        m_ui->colourSelector->clear();
        for (auto it = ColourHelper::baseBegin, end = ColourHelper::compositeEnd; it != end; ++it)
            m_ui->colourSelector->addItem(QString::fromStdString(ColourHelper::toString(*it)));
    }
    else if (button->text().compare("Gate") == 0)
    {
        m_ui->grid->currentMode = HexagonalGrid::Mode::Gate;
        m_ui->colourSelector->setEnabled(true);
        m_ui->colourSelector->clear();
        for (auto it = ColourHelper::baseBegin, end = ColourHelper::compositeEnd; it != end; ++it)
            m_ui->colourSelector->addItem(QString::fromStdString(ColourHelper::toString(*it)));
    }
    else if (button->text().compare("Switch-Bridge") == 0)
    {
        m_ui->grid->currentMode = HexagonalGrid::Mode::SwitchBridge;
        m_ui->colourSelector->setEnabled(false);
    }
    else if (button->text().compare("Activator-Bridge") == 0)
    {
        m_ui->grid->currentMode = HexagonalGrid::Mode::ActivatorBridge;
        m_ui->colourSelector->setEnabled(false);
    }
    else if (button->text().compare("Activator") == 0)
    {
        m_ui->grid->currentMode = HexagonalGrid::Mode::Activator;
        m_ui->colourSelector->setEnabled(false);
    }
    else if (button->text().compare("Destination") == 0)
    {
        m_ui->grid->currentMode = HexagonalGrid::Mode::Destination;
        m_ui->colourSelector->setEnabled(false);
    }
    else if (button->text().compare("Painter") == 0)
    {
        m_ui->grid->currentMode = HexagonalGrid::Mode::Painter;
        m_ui->colourSelector->setEnabled(true);
        m_ui->colourSelector->clear();
        for (auto it = ColourHelper::baseBegin, end = ColourHelper::baseEnd; it != end; ++it)
            m_ui->colourSelector->addItem(QString::fromStdString(ColourHelper::toString(*it)));
    }
    else if (button->text().compare("Delete") == 0)
    {
        m_ui->grid->currentMode = HexagonalGrid::Mode::Delete;
        m_ui->colourSelector->setEnabled(false);
    }

    //If previousColour still available then select it
    if (m_ui->colourSelector->currentText().compare(previousColour) != 0
            && m_ui->colourSelector->findText(previousColour) != -1)
        m_ui->colourSelector->setCurrentIndex(m_ui->colourSelector->findText(previousColour));
}


void WorldDesigner::colourSelectorChanged(const QString &text)
{
    for (const auto col: ColourHelper::Colours)
    {
        if (text.compare(QString::fromStdString(ColourHelper::toString(col))) == 0)
        {
            //Change colour of bridge, gate, creature, and painter
            if (col != m_ui->grid->currentColour)
            {
                //Bridge and gate
                if (col != Colour::Uncoloured)
                {
                    if (m_ui->connectionButton->text().compare("Bridge") == 0)
                        m_ui->connectionButton->setIcon(QIcon(ImageLoader::loadBridge(col)));
                    else if (m_ui->connectionButton->text().compare("Gate") == 0)
                        m_ui->connectionButton->setIcon(QIcon(ImageLoader::loadGate(col)));
                    m_connectionMenu->actions().at(0)->setIcon(QIcon(ImageLoader::loadBridge(col)));
                    m_connectionMenu->actions().at(1)->setIcon(QIcon(ImageLoader::loadGate(col)));
                }
                //Painter
                if (col == Colour::Red || col == Colour::Yellow || col == Colour::Blue)
                {
                    m_ui->painterButton->setIcon(QIcon(ImageLoader::loadPainter(col)));
                }
                //Creature
                m_ui->creatureButton->setIcon(QIcon(ImageLoader::loadCreature(col)));

                m_ui->grid->currentColour = col;
            }
            break;
        }
    }
}

void WorldDesigner::toolBarTriggered(QAction *action)
{
    if (action->text().compare("Solve") == 0)
        m_ui->grid->solve();
    else if (action->text().compare("Clear") == 0)
        m_ui->grid->clear();
    else if (action->text().compare("Save") == 0)
    {
        const QString filename = QFileDialog::getSaveFileName(this, "Save World", "world.dat",
                                                        "Dat (*.dat)");
        const QString error = m_ui->grid->saveToFile(filename);
        if (!error.isEmpty())
            QMessageBox::information(this, tr("Unable to open file"), error);
    }
    else if (action->text().compare("Load") == 0)
    {
        const QString filename = QFileDialog::getOpenFileName(this, "Save World", "world.dat",
                                                        "Dat (*.dat)");
        const QString error = m_ui->grid->loadFromFile(filename);
        if (!error.isEmpty())
            QMessageBox::information(this, tr("Unable to open file"), error);
    }
    else if (action->text().compare("Debug") == 0)
        m_ui->grid->setDebug(action->isChecked());
}

WorldDesigner::~WorldDesigner()
{
    m_connectionMenu.reset();
    m_ui.reset();
}
