#include "hexagonalgrid.h"

#include <QWidget>
#include <vector>
#include <memory>
#include <optional>
#include <QPainter>
#include <QMouseEvent>
#include <cmath>

#include "path.h"
#include "imageloader.h"

HexagonalGrid::HexagonalGrid(QWidget *parent)
    : QWidget{parent}, currentMode{Mode::Island}, currentColour{Colour::Uncoloured},
      HEXAGON_OFFSET_H{HEXAGON_SIDE_LENGTH * std::cos(M_PI/6) * 2},
      HEXAGON_OFFSET_W{HEXAGON_SIDE_LENGTH * std::sin(M_PI/6) + HEXAGON_SIDE_LENGTH},
      m_logic{}, m_selectedIsland{std::nullopt}
{
    //Creates a hexagon and half hexagon
    QTransform transform;
    transform.rotate(-30);
    const QPointF point{0, -HEXAGON_SIDE_LENGTH};
    for (int angle = -30; angle < 360; angle += 60)
    {
        m_hexagon.append(transform.map(point));
        if (angle < 180)
            m_halfHexagon.append(transform.map(point));
        transform.rotate(60);
    }
}

//Removes everything from grid
void HexagonalGrid::clear()
{
    for (auto &col: m_logic.m_grid)
    {
        for (auto cell: col)
        {
            cell->removeTopLevel();
            cell->removeTopLevel();
        }
    }
    m_logic.m_connections.clear();
    m_selectedIsland = {-1, -1};
    m_selectedActivatorBridge.reset();

    update();
}

//Attempts to save the current grid state to a file of the given name
//Returns error string, if exists
QString HexagonalGrid::saveToFile(QString filename) const
{
    return m_logic.saveToFile(filename);
}

//Attempts to load a grid state from a file of the given name
//Returns error string, if exists
QString HexagonalGrid::loadFromFile(QString filename)
{
    return m_logic.loadFromFile(filename);
}

//Sets debug output state
void HexagonalGrid::setDebug(bool state)
{
    m_logic.debug = state;
}

//Converts grid to a solver world and solves it
//Returns result
std::optional<Path> HexagonalGrid::solve() const
{
    return m_logic.convertToSolver().run();
}

//Returns the cell coordinates of the cell at the given position, if one exists
std::optional<std::pair<size_t, size_t>> HexagonalGrid::findCellAt(const QPoint &pos)
{
    bool cellFound = false;
    for (size_t col = 0; col < m_logic.m_hexagonCols && !cellFound; ++col)
    {
        for (size_t row = 0; row < m_logic.m_hexagonRows && !cellFound; ++row)
        {
            const QPointF offset{col * HEXAGON_OFFSET_W,
                        row * HEXAGON_OFFSET_H + ((col % 2 == 1) ? HEXAGON_OFFSET_H/2 : 0)};
            const QPolygonF curHexagon{m_hexagon.translated(offset)};

            if (curHexagon.containsPoint(pos, Qt::FillRule::WindingFill))
                return std::pair<size_t, size_t>(col, row);
        }
    }
    return std::nullopt;
}

//Returns the straight path between start and end cells of the expected length, if exists
std::vector<std::pair<size_t, size_t>> HexagonalGrid::findPath(const std::pair<size_t, size_t> start,
                                                         const std::pair<size_t, size_t> end,
                                                         const size_t expectedLength)
{
    const long long xDiff = static_cast<long long>(end.first) - static_cast<long long>(start.first);
    const long long yDiff = static_cast<long long>(end.second) - static_cast<long long>(start.second);
    //Determines direction of end cell from start cell
    HexagonalDirection direction;
    if (xDiff == 0 && yDiff < 0)
        direction = HexagonalDirection::North;
    else if (xDiff == 0 && yDiff > 0)
        direction = HexagonalDirection::South;
    else if (xDiff > 0 && (yDiff < 0 || ((start.first % 2 == 1) && yDiff == 0)))
        direction = HexagonalDirection::NorthEast;
    else if (xDiff > 0 && (yDiff > 0 || ((start.first % 2 == 0) && yDiff == 0)))
        direction = HexagonalDirection::SouthEast;
    else if (xDiff < 0 && (yDiff > 0 || ((start.first % 2 == 0) && yDiff == 0)))
        direction = HexagonalDirection::SouthWest;
    else if (xDiff < 0 && (yDiff < 0 || ((start.first % 2 == 1) && yDiff == 0)))
        direction = HexagonalDirection::NorthWest;
    else
        return {};

    std::pair<size_t, size_t> currentCell = start;
    std::vector<std::pair<size_t, size_t>> path;

    while (path.size() < expectedLength)
    {
        //Move in direction by one cell
        auto nextCell = m_logic.findCell(currentCell, direction, 1);
        if (nextCell)
            currentCell = nextCell.value();
        else
            return {};

        path.push_back(currentCell);
    }

    //If end cell reached then return path
    if (currentCell == end)
        return path;
    else
        return {};
}

void HexagonalGrid::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    //Draw background
    painter.fillRect(0, 0, size().width(), size().height(), Qt::black);

    //Draw hexagonal grid
    painter.setPen(QPen(QBrush(Qt::white), 1, Qt::PenStyle::DashLine));
    for (size_t col = 0; col < m_logic.m_hexagonCols; ++col)
    {
        for (size_t row = 0; row < m_logic.m_hexagonRows; ++row)
        {
            const QPointF offset{col * HEXAGON_OFFSET_W,
                        row * HEXAGON_OFFSET_H + ((col % 2 == 1) ? HEXAGON_OFFSET_H/2 : 0)};

            painter.drawPolyline(QPolygonF(m_halfHexagon).translated(offset));
        }
    }

    //Draw islands, creatures, destinations, and activators
    for (size_t col = 0; col < m_logic.m_hexagonCols; ++col)
    {
        for (size_t row = 0; row < m_logic.m_hexagonRows; ++row)
        {
            if (m_logic.m_grid.at(col).at(row) && m_logic.m_grid.at(col).at(row)->isIsland())
            {
                //Draw island
                painter.setBrush(QBrush(QColor(0, 127, 0), Qt::BrushStyle::SolidPattern));
                painter.setPen(QPen(painter.brush(), 0, Qt::PenStyle::NoPen));
                const QPointF offset{col * HEXAGON_OFFSET_W,
                            row * HEXAGON_OFFSET_H + ((col % 2 == 1) ? HEXAGON_OFFSET_H/2 : 0)};
                const QPolygonF island{m_hexagon.translated(offset)};
                painter.drawPolygon(island);

                //Draw object on island
                QImage img;
                if (m_logic.m_grid.at(col).at(row)->hasCreature() != Colour::Invalid)
                    img.load(ImageLoader::loadCreature(m_logic.m_grid.at(col).at(row)->hasCreature()));
                else if (m_logic.m_grid.at(col).at(row)->hasPainter() != Colour::Invalid)
                    img.load(ImageLoader::loadPainter(m_logic.m_grid.at(col).at(row)->hasPainter()));
                else if (m_logic.m_grid.at(col).at(row)->hasDestination())
                    img.load(ImageLoader::destinationPath);
                else if (!m_logic.m_grid.at(col).at(row)->hasActivator().expired())
                {
                    img.load(ImageLoader::activatorPath);
                    if (m_logic.m_grid.at(col).at(row)->hasActivator().lock() == m_selectedActivatorBridge.lock())
                    {
                        painter.setBrush(QBrush(Qt::blue, Qt::BrushStyle::SolidPattern));
                        painter.setPen(QPen(painter.brush(), 2, Qt::PenStyle::SolidLine));
                        const QPointF offset{col * HEXAGON_OFFSET_W,
                                    row * HEXAGON_OFFSET_H
                                    + ((col % 2 == 1) ? HEXAGON_OFFSET_H/2 : 0)};
                        QTransform shrink;
                        shrink.scale(0.95, 0.95);
                        QPolygonF hex = shrink.map(m_hexagon).translated(offset);
                        painter.drawPolyline(hex);
                    }
                }

                if (!img.isNull())
                    painter.drawImage(island.boundingRect(), img);
            }
        }
    }

    //Draw connections
    for (const auto &connection: m_logic.m_connections)
    {
        const auto p_connection = connection.lock();

        //Get bounding box for connection area
        QRectF connectionArea;
        {
            const QPointF offset{0, -HEXAGON_OFFSET_H};
            QPolygonF firstCell{m_hexagon.translated(offset)};
            if (p_connection->getCellSize() == 4)
            {
                const QPointF offset2{0, -2 * HEXAGON_OFFSET_H};
                firstCell = firstCell.united(QPolygonF(m_hexagon).translated(offset2));
            }
            firstCell.translate(-m_hexagon.boundingRect().center());
            connectionArea = firstCell.boundingRect();
        }

        //Use connection direction to determine rotation
        double rotation = 0;
        switch(p_connection->getDirection())
        {
        case HexagonalDirection::NorthEast:
            rotation = 60;
            break;
        case HexagonalDirection::SouthEast:
            rotation = 120;
            break;
        case HexagonalDirection::South:
            rotation = 180;
            break;
        case HexagonalDirection::SouthWest:
            rotation = 240;
            break;
        case HexagonalDirection::NorthWest:
            rotation = 300;
            break;
        default: break;
        }

        //Load corresponding image
        QImage img;
        switch (p_connection->m_type)
        {
        case ConnectionType::Bridge:
            img = QImage(ImageLoader::loadBridge(p_connection->m_colour));
            break;
        case ConnectionType::Gate:
            img = QImage(ImageLoader::loadGate(p_connection->m_colour));
            break;
        case ConnectionType::SwitchBridge:
            img = QImage(ImageLoader::switchBridgePath);
            break;
        case ConnectionType::ActivatorBridge:
            if (p_connection == m_selectedActivatorBridge.lock())
                img = QImage(ImageLoader::loadActivatorBridge(true));
            else
                img = QImage(ImageLoader::loadActivatorBridge(false));
            break;
        default:
            painter.drawRect(connectionArea);
            break;
        }

        //Transforms painter so that start cell is at origin and connection is north of origin
        //Draws connection, then reverses transform
        const std::pair<size_t, size_t> startCell = p_connection->getCellAt(0)->pos;
        QPointF translateBy{startCell.first * HEXAGON_OFFSET_W,
                    startCell.second * HEXAGON_OFFSET_H
                    + ((startCell.first % 2 == 1) ? HEXAGON_OFFSET_H/2 : 0)};
        translateBy = translateBy + m_hexagon.boundingRect().center();
        painter.translate(translateBy);
        painter.rotate(rotation);
        painter.drawImage(connectionArea, img);
        painter.rotate(-rotation);
        painter.translate(-translateBy);
    }

    //Draw outline on selected island
    if (m_selectedIsland)
    {
        painter.setBrush(QBrush(Qt::red, Qt::BrushStyle::SolidPattern));
        painter.setPen(QPen(painter.brush(), 2, Qt::PenStyle::SolidLine));
        const QPointF offset{m_selectedIsland->first * HEXAGON_OFFSET_W,
                    m_selectedIsland->second * HEXAGON_OFFSET_H +
                    ((m_selectedIsland->first % 2 == 1) ? HEXAGON_OFFSET_H/2 : 0)};
        QPolygonF hex{m_hexagon.translated(offset)};
        painter.drawPolyline(hex);
    }
}

void HexagonalGrid::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        const auto cellPos = findCellAt(event->pos());
        if (cellPos && m_logic.m_grid.at(cellPos->first).at(cellPos->second))
        {
            if (currentMode == Mode::Island)
            {
                //Add Island to cell
                m_logic.m_grid.at(cellPos->first).at(cellPos->second)->addIsland();
            }
            else if (currentMode == Mode::Creature)
            {
                //Add creature to island
                m_logic.m_grid.at(cellPos->first).at(cellPos->second)->addCreature(currentColour);
            }
            else if (currentMode == Mode::Bridge || currentMode == Mode::Gate ||
                     currentMode == Mode::SwitchBridge || currentMode == Mode::ActivatorBridge)
            {
                //Select island
                if (m_logic.m_grid.at(cellPos->first).at(cellPos->second)->isIsland())
                    m_selectedIsland = cellPos;
                //Switch switch-bridge direction
                else if (currentMode == Mode::SwitchBridge &&
                         m_logic.m_grid.at(cellPos->first).at(cellPos->second)->isConnection())
                {
                    const auto connection =*m_logic.m_grid.at(cellPos->first).at(cellPos->second)->
                            getConnectionsBegin();
                    if (connection->m_type == ConnectionType::SwitchBridge)
                        connection->switchDirection();
                }
                //Deselect island
                else if (m_selectedIsland)
                    m_selectedIsland = std::nullopt;
            }
            else if (currentMode == Mode::Activator)
            {
                //Select clicked activator-bridge
                if (m_logic.m_grid.at(cellPos->first).at(cellPos->second)->isConnection())
                    m_selectedActivatorBridge =
                            *m_logic.m_grid.at(cellPos->first).at(cellPos->second)->
                            getConnectionsBegin();
                //Select activator-bridge for clicked activator
                else if (!m_logic.m_grid.at(cellPos->first).at(cellPos->second)->hasActivator().expired())
                    m_selectedActivatorBridge =
                            m_logic.m_grid.at(cellPos->first).at(cellPos->second)->hasActivator();
                //Add activator to clicked island if an activator-bridge is selected
                else if (m_logic.m_grid.at(cellPos->first).at(cellPos->second)->isIsland()
                         && !m_selectedActivatorBridge.expired())
                    m_logic.m_grid.at(cellPos->first).at(cellPos->second)->
                            addActivator(m_selectedActivatorBridge.lock());
                //Deselect activator-bridge
                else if (!m_selectedActivatorBridge.expired())
                    m_selectedActivatorBridge.reset();
            }
            else if (currentMode == Mode::Destination)
            {
                //Add Destination to island
                m_logic.m_grid.at(cellPos->first).at(cellPos->second)->addDestination();
            }
            else if (currentMode == Mode::Painter)
            {
                //Add Painter to island
                m_logic.m_grid.at(cellPos->first).at(cellPos->second)->addPainter(currentColour);
            }
            else if (currentMode == Mode::Delete)
            {
                m_logic.m_grid.at(cellPos->first).at(cellPos->second)->removeTopLevel();
                if (cellPos == m_selectedIsland &&
                        !m_logic.m_grid.at(cellPos->first).at(cellPos->second)->isIsland())
                    m_selectedIsland = {-1,-1};

                //Forget any deleted connections
                for (auto it = m_logic.m_connections.begin(); it != m_logic.m_connections.end();)
                {
                    if (it->expired())
                        it = m_logic.m_connections.erase(it);
                    else
                        ++it;
                }
            }

            update();
        }
    }
    else if (event->button() == Qt::RightButton)
    {
        const auto cellPos = findCellAt(event->pos());
        if (cellPos && m_selectedIsland
                && m_logic.m_grid.at(cellPos->first).at(cellPos->second)->isIsland())
        {
            if (currentMode == Mode::Gate || currentMode == Mode::Bridge
                    || currentMode == Mode::SwitchBridge || currentMode == Mode::ActivatorBridge)
            {
                std::vector<std::pair<size_t, size_t>> path = findPath(m_selectedIsland.value(),
                                                                 cellPos.value(),
                                                                 3 - (currentMode == Mode::Gate));
                //If there exists a valid path between the cells then create connection
                if (!path.empty())
                {
                    std::vector<std::shared_ptr<HexagonalCell>> cells;
                    cells.push_back(m_logic.m_grid.at(m_selectedIsland->first).at(m_selectedIsland->second));
                    for (const auto &cell: path)
                        cells.push_back(m_logic.m_grid.at(cell.first).at(cell.second));

                    //Create connection
                    std::shared_ptr<HexagonalCellConnection> connection;
                    switch (currentMode)
                    {
                    case Mode::Gate:
                        connection = std::make_shared<HexagonalCellConnection>(
                                    ConnectionType::Gate,currentColour, cells);
                        break;
                    case Mode::Bridge:
                        connection = std::make_shared<HexagonalCellConnection>(
                                    ConnectionType::Bridge, currentColour, cells);
                        break;
                    case Mode::SwitchBridge:
                        connection = std::make_shared<HexagonalCellConnection>(
                                    ConnectionType::SwitchBridge, currentColour, cells);
                        break;
                    case Mode::ActivatorBridge:
                        connection = std::make_shared<HexagonalCellConnection>(
                                    ConnectionType::ActivatorBridge, currentColour, cells);
                        break;
                    default: break;
                    }
                    connection->initialise();
                    m_logic.m_connections.push_back(connection);
                }
            }

            update();
        }
    }
}

//Called when grid container is resized, updates grid dimension to fit
void HexagonalGrid::resizeEvent(QResizeEvent *)
{
    size_t oldCols = m_logic.m_hexagonCols;
    size_t oldRows = m_logic.m_hexagonRows;
    m_logic.m_hexagonCols = static_cast<size_t>(size().width() / HEXAGON_OFFSET_W) + 1;
    m_logic.m_hexagonRows = static_cast<size_t>(size().height() / HEXAGON_OFFSET_H) + 2;
    m_logic.m_grid.resize(m_logic.m_hexagonCols);
    for (auto &col: m_logic.m_grid)
        col.resize(m_logic.m_hexagonRows);

    //Add cell position to new cells
    for (size_t x = oldCols; x < m_logic.m_hexagonCols; ++x)
        for (size_t y = 0; y < oldRows; ++y)
        {
            m_logic.m_grid.at(x).at(y) = std::make_shared<HexagonalCell>();
            m_logic.m_grid.at(x).at(y)->pos = {x, y};
        }

    for (size_t x = 0; x < m_logic.m_hexagonCols; ++x)
        for (size_t y = oldRows; y < m_logic.m_hexagonRows; ++y)
        {
            m_logic.m_grid.at(x).at(y) = std::make_shared<HexagonalCell>();
            m_logic.m_grid.at(x).at(y)->pos = {x, y};
        }

    //Remove connections that are out-of-bounds
    if (oldCols > m_logic.m_hexagonCols || oldRows > m_logic.m_hexagonRows)
    {
        for (auto it = m_logic.m_connections.begin(); it != m_logic.m_connections.end();)
        {
            auto connection = it->lock();
            //If any cell is out-of-bounds then so is connection
            bool outOfBounds = false;
            for (size_t i = 0; i < connection->getCellSize(); ++i)
            {
                auto pos = connection->getCellAt(i)->pos;
                if (pos.first >= m_logic.m_hexagonCols || pos.second >= m_logic.m_hexagonRows)
                {
                    outOfBounds = true;
                    break;
                }
            }

            //If out-of-bounds then delete connection
            if (outOfBounds)
            {
                connection->remove();
                it = m_logic.m_connections.erase(it);
            }
            else
                ++it;
        }
    }
}
