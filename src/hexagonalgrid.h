#ifndef HEXAGONALGRID_H
#define HEXAGONALGRID_H

#include <QWidget>
#include <vector>
#include <memory>

#include "colour.h"
#include "hexagonalcellconnection.h"
#include "path.h"
#include "hexagonalgridlogic.h"

class HexagonalGrid : public QWidget
{
    Q_OBJECT
public:
    enum class Mode {Island, Creature, Bridge, Gate, SwitchBridge, ActivatorBridge, Activator,
                    Destination, Painter, Delete};
    explicit HexagonalGrid(QWidget *parent = nullptr);

    void clear();
    QString saveToFile(QString filename) const;
    QString loadFromFile(QString filename);

    void setDebug(bool state);
    std::optional<Path> solve() const;

    Mode currentMode;
    Colour currentColour;

private:
    //Defines hexagon size
    const double HEXAGON_SIDE_LENGTH{45};
    const double HEXAGON_OFFSET_H;
    const double HEXAGON_OFFSET_W;

    QPolygonF m_hexagon, m_halfHexagon;

    HexagonalGridLogic m_logic;

    std::optional<std::pair<size_t, size_t>> m_selectedIsland;
    std::weak_ptr<HexagonalCellConnection> m_selectedActivatorBridge;

    std::optional<std::pair<size_t, size_t>> findCellAt(const QPoint &pos);
    std::vector<std::pair<size_t, size_t>> findPath(const std::pair<size_t, size_t> start,
                                              const std::pair<size_t, size_t> end,
                                              const size_t expectedLength);

    void paintEvent(QPaintEvent *);
    void mousePressEvent(QMouseEvent *event);
    void resizeEvent(QResizeEvent *);
};

#endif // HEXAGONALGRID_H
