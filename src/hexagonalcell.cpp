#include "hexagonalcell.h"

#include <memory>
#include <vector>
#include <algorithm>

#include "colour.h"
#include "hexagonalcellconnection.h"

HexagonalCell::HexagonalCell()
    : m_island{false}, m_destination{false}, m_creature{Colour::Invalid},
      m_painter{Colour::Invalid}, m_activator{}, m_connections{} {}

//Returns whether or not this cell is an island
bool HexagonalCell::isIsland() const
{
    return m_island;
}

//Attempts to add an island
//Fails if cell is a connection
void HexagonalCell::addIsland()
{
    if (m_connections.empty())
        m_island = true;
}

//Returns whether or not this cell has a destination
bool HexagonalCell::hasDestination() const
{
    return m_destination;
}

//Attempts to add destination to cell
//Fails if cell is not an island
void HexagonalCell::addDestination()
{
    if (m_island && m_creature == Colour::Invalid && m_painter == Colour::Invalid &&
            m_activator.expired())
        m_destination = true;
}

//Returns colour of creature on cell
//If no creature returns invalid colour
Colour HexagonalCell::hasCreature() const
{
    return m_creature;
}

//Attempts to add creature of given colour to cell
//Fails if cell is not an island
void HexagonalCell::addCreature(const Colour t_colour)
{
    if (m_island && !m_destination && m_painter == Colour::Invalid && m_activator.expired())
        m_creature = t_colour;
}

//Returns colour of painter
//If no painter returns invalid colour
Colour HexagonalCell::hasPainter() const
{
    return m_painter;
}

//Attempts to add painter of given colour to cell
//Fails if cell is not an island
void HexagonalCell::addPainter(const Colour t_colour)
{
    if (m_island && !m_destination && m_creature == Colour::Invalid && m_activator.expired() &&
        (t_colour == Colour::Red || t_colour == Colour::Yellow || t_colour == Colour::Blue))
        m_painter = t_colour;
}

//If no activator returns nullptr
//Else returns pointer to connected activator-bridge
std::weak_ptr<HexagonalCellConnection> HexagonalCell::hasActivator() const
{
    return m_activator;
}

//Attempts to add activator of given connection to cell
//Fails if cell is not an island
void HexagonalCell::addActivator(std::weak_ptr<HexagonalCellConnection> t_connection)
{
    if (m_island && !m_destination && m_creature == Colour::Invalid &&
            m_creature == Colour::Invalid)
        m_activator = t_connection;
}

//Returns whether or not this cell is a connection
bool HexagonalCell::isConnection() const
{
    return (!m_island && !m_connections.empty());
}

//Returns begin iterator for connections
std::vector<std::shared_ptr<HexagonalCellConnection>>::iterator HexagonalCell::getConnectionsBegin()
{
    return m_connections.begin();
}

//Returns end iterator for connections
std::vector<std::shared_ptr<HexagonalCellConnection>>::iterator HexagonalCell::getConnectionsEnd()
{
    return m_connections.end();
}

//Attempts to add connection to cell
//Fails if cell is not an island and already has a connection (cell is a connection)
void HexagonalCell::addConnection(std::shared_ptr<HexagonalCellConnection> t_connection)
{
    if (m_island || m_connections.empty())
        m_connections.push_back(t_connection);
}

//Removes connection at given iterator from cell
void HexagonalCell::removeConnection(std::shared_ptr<HexagonalCellConnection> t_connection)
{
    m_connections.erase(std::remove(m_connections.begin(), m_connections.end(), t_connection),
                        m_connections.end());
}

//Removes the top level of object from cell
//If cell is a connection, removes connection
//If cell is an island with something on it (not connections) removes that something
//If cell is an island with nothing on it removes island and any connections
void HexagonalCell::removeTopLevel()
{
    if (m_island)
    {
        if (m_destination)
            m_destination = false;
        else if (m_creature != Colour::Invalid)
            m_creature = Colour::Invalid;
        else if (m_painter != Colour::Invalid)
            m_painter = Colour::Invalid;
        else if (!m_activator.expired())
            m_activator.reset();
        else
        {
            m_island = false;
            while (!m_connections.empty())
            {
                std::shared_ptr<HexagonalCellConnection> tmp = m_connections.front();
                tmp->remove();
                tmp.reset();
            }
        }
    }
    else if (!m_connections.empty())
    {
        std::shared_ptr<HexagonalCellConnection> tmp = m_connections.front();
        tmp->remove();
        tmp.reset();
    }
}
