#include "colour.h"

#include <vector>
#include <optional>
#include <string>

const std::vector<Colour>
ColourHelper::Colours = {Colour::Uncoloured, Colour::Red, Colour::Yellow, Colour::Blue,
                         Colour::Orange, Colour::Purple, Colour::Green};
const std::vector<Colour>::const_iterator ColourHelper::baseBegin = Colours.cbegin()+1;
const std::vector<Colour>::const_iterator ColourHelper::baseEnd = baseBegin+3;
const std::vector<Colour>::const_iterator ColourHelper::compositeBegin = baseEnd;
const std::vector<Colour>::const_iterator ColourHelper::compositeEnd = compositeBegin+3;

//Returns the combination of two given colours, if combination not valid returns colour invalid
Colour ColourHelper::combine(const Colour t_col1, const Colour t_col2)
{
    if (t_col1 == Colour::Red)
    {
        if (t_col2 == Colour::Yellow)
            return Colour::Orange;
        else if (t_col2 == Colour::Blue)
            return Colour::Purple;
    }
    else if (t_col1 == Colour::Yellow)
    {
        if (t_col2 == Colour::Red)
            return Colour::Orange;
        else if (t_col2 == Colour::Blue)
            return Colour::Green;
    }
    else if (t_col1 == Colour::Blue)
    {
        if (t_col2 == Colour::Red)
            return Colour::Purple;
        else if (t_col2 == Colour::Yellow)
            return Colour::Green;
    }
    return Colour::Invalid;
}

//Returns the split of the given colour, if exists
std::optional<std::pair<Colour,Colour>> ColourHelper::split(const Colour t_colour)
{
    if (t_colour == Colour::Orange)
        return std::pair<Colour,Colour>(Colour::Red, Colour::Yellow);
    else if (t_colour == Colour::Purple)
        return std::pair<Colour,Colour>(Colour::Red, Colour::Blue);
    else if (t_colour == Colour::Green)
        return std::pair<Colour,Colour>(Colour::Yellow, Colour::Blue);
    else
        return std::nullopt;
}

//Returns colour corresponding to given index
//Allows for enum Colours to be iterated
Colour ColourHelper::fromUInt(const unsigned int t_uint)
{
    if (t_uint > Colours.size())
        return Colour::Invalid;
    return static_cast<Colour>(t_uint);
}

//Returns string representing given colour's name
std::string ColourHelper::toString(const Colour t_colour)
{
    switch (t_colour)
    {
    case Colour::Uncoloured: return "Uncoloured";
    case Colour::Red: return "Red";
    case Colour::Yellow: return "Yellow";
    case Colour::Blue: return "Blue";
    case Colour::Orange: return "Orange";
    case Colour::Purple: return "Purple";
    case Colour::Green: return "Green";
    default: return "Invalid";
    }
}
