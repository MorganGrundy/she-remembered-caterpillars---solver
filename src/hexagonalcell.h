#ifndef HEXAGONALCELL_H
#define HEXAGONALCELL_H

#include <vector>
#include <memory>

#include "colour.h"
#include "hexagonalcellconnection.h"

class HexagonalCell
{
public:
    HexagonalCell();
    std::pair<size_t, size_t> pos;

    bool isIsland() const;
    void addIsland();

    bool hasDestination() const;
    void addDestination();

    Colour hasCreature() const;
    void addCreature(const Colour t_colour);

    Colour hasPainter() const;
    void addPainter(const Colour t_colour);

    std::weak_ptr<HexagonalCellConnection> hasActivator() const;
    void addActivator(std::weak_ptr<HexagonalCellConnection> t_connection);

    bool isConnection() const;
    std::vector<std::shared_ptr<HexagonalCellConnection>>::iterator getConnectionsBegin();
    std::vector<std::shared_ptr<HexagonalCellConnection>>::iterator getConnectionsEnd();
    void addConnection(std::shared_ptr<HexagonalCellConnection> t_connection);
    void removeConnection(std::shared_ptr<HexagonalCellConnection> t_connection);

    void removeTopLevel();

private:
    bool m_island;
    bool m_destination;

    Colour m_creature;
    Colour m_painter;

    std::weak_ptr<HexagonalCellConnection> m_activator;

    std::vector<std::shared_ptr<HexagonalCellConnection>> m_connections;
};

#endif // HEXAGONALCELL_H
