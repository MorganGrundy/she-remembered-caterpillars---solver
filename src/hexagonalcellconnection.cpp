#include "hexagonalcellconnection.h"

#include <algorithm>
#include <vector>
#include <memory>

#include "colour.h"
#include "connectiontype.h"
#include "hexagonalcell.h"

HexagonalCellConnection::HexagonalCellConnection(ConnectionType t_type, Colour t_colour,
                                                std::vector<std::shared_ptr<HexagonalCell>> t_cells)
    : m_type{t_type}, m_colour{t_colour}, m_cells{t_cells}
{
    //Uses start and end cells to calculate connection direction
    const long long xDiff = static_cast<long long>(m_cells.back()->pos.first) -
            static_cast<long long>(m_cells.front()->pos.first);
    const long long yDiff = static_cast<long long>(m_cells.back()->pos.second) -
            static_cast<long long>(m_cells.front()->pos.second);

    if (xDiff == 0 && yDiff < 0)
        m_direction = HexagonalDirection::North;
    else if (xDiff == 0 && yDiff > 0)
        m_direction = HexagonalDirection::South;
    else if (xDiff > 0 && yDiff < 0)
        m_direction = HexagonalDirection::NorthEast;
    else if (xDiff > 0 && yDiff > 0)
        m_direction = HexagonalDirection::SouthEast;
    else if (xDiff < 0 && yDiff < 0)
        m_direction = HexagonalDirection::NorthWest;
    else if (xDiff < 0 && yDiff > 0)
        m_direction = HexagonalDirection::SouthWest;
}

//Needs to be called after constructor to add connection to cells
void HexagonalCellConnection::initialise()
{
    for (auto cell: m_cells)
        cell->addConnection(shared_from_this());
}

//Returns direction
HexagonalDirection HexagonalCellConnection::getDirection() const
{
    return m_direction;
}

//Removes this connection from all it's cells
void HexagonalCellConnection::remove()
{
    for (auto cell: m_cells)
        cell->removeConnection(shared_from_this());
}

size_t HexagonalCellConnection::getCellSize() const
{
    return m_cells.size();
}

std::shared_ptr<HexagonalCell> HexagonalCellConnection::getCellAt(size_t t_index)
{
    if (t_index >= m_cells.size())
        return nullptr;
    else
        return m_cells.at(t_index);
}

void HexagonalCellConnection::switchDirection()
{
    std::reverse(m_cells.begin(), m_cells.end());
    m_direction = static_cast<HexagonalDirection>((static_cast<unsigned int>(m_direction) + 3) % 6);
}
