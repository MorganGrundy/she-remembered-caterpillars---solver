#include "connection.h"

#include <memory>

#include "colour.h"
#include "creature.h"
#include "connectiontype.h"
#include "connection.h"
#include "island.h"

size_t Connection::nextID = 0;

//Constructor
Connection::Connection()
    : type{ConnectionType::Undefined} {}

//Constructor
Connection::Connection(const std::shared_ptr<Island> t_island1,
                       const std::shared_ptr<Island> t_island2,
                       const ConnectionType t_type)
    : p_island1{t_island1}, p_island2{t_island2}, type{t_type}, id{nextID++} {}

void Connection::updateIslands()
{
    p_island1->connections.push_back(shared_from_this());
    p_island2->connections.push_back(shared_from_this());
}

//Constructor
Bridge::Bridge(const std::shared_ptr<Island> t_island1, const std::shared_ptr<Island> t_island2,
               const Colour t_colour)
    : Connection{t_island1, t_island2, ConnectionType::Bridge}, colour{t_colour} {}

//Return if the creature can cross the bridge
bool Bridge::canCross(const std::shared_ptr<Creature> t_creature) const
{
    //Uncoloured
    if (t_creature->colour == Colour::Uncoloured)
        return false;
    //Bridge is a base colour
    else if (colour < *ColourHelper::baseEnd)
        return (t_creature->colour == colour ||
                (t_creature->p_joinedCreature && t_creature->p_joinedCreature->colour == colour));
    //Bridge is a composite colour, only joined creatures of same colour of bridge can cross
    else
        return (t_creature->p_joinedCreature && t_creature->joinedColour == colour);
}

//Constructor
Gate::Gate(const std::shared_ptr<Island> t_island1, const std::shared_ptr<Island> t_island2,
           const Colour t_colour)
    : Connection{t_island1, t_island2, ConnectionType::Gate}, colour{t_colour} {}

//Return if the creature can cross the gate
bool Gate::canCross(const std::shared_ptr<Creature> t_creature) const
{
    //Uncoloured
    if (t_creature->colour == Colour::Uncoloured)
        return true;
    //Gate is a base colour
    else if (colour < *ColourHelper::baseEnd)
        return t_creature->colour != colour &&
                (!t_creature->p_joinedCreature || t_creature->p_joinedCreature->colour != colour);
    //Gate is a composite colour
    else
    {
        //Base creature
        if (!t_creature->p_joinedCreature)
        {
            std::optional<std::pair<Colour,Colour>> splitGate = ColourHelper::split(colour);
            return (t_creature->colour != splitGate->first &&
                    t_creature->colour != splitGate->second);
        }
        //Composite creature
        else
            return false;
    }
}

//Constructor
SwitchBridge::SwitchBridge(const std::shared_ptr<Island> t_island1,
                           const std::shared_ptr<Island> t_island2)
    : Connection{t_island1, t_island2, ConnectionType::SwitchBridge} {}

//Return if the creature can cross the switch-bridge
bool SwitchBridge::canCross(const std::shared_ptr<Island> t_island,
                            const Colour t_creatureColour) const
{
    return (t_island == p_island1 &&
            (t_creatureColour == Colour::Red || t_creatureColour == Colour::Yellow ||
             t_creatureColour == Colour::Blue || t_creatureColour == Colour::Uncoloured));
}

//Constructor
ActivatorBridge::ActivatorBridge(const std::shared_ptr<Island> t_island1,
                                 const std::shared_ptr<Island> t_island2)
    : Connection{t_island1, t_island2, ConnectionType::ActivatorBridge} {}
