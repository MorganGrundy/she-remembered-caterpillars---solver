#ifndef ACTION_H
#define ACTION_H

#include <memory>

#include "colour.h"

class Creature;
class Connection;
class Island;

enum class ActionType {Undefined, Join, Split, Painter, Connection};

class Action
{
public:
    enum class JoinOrSplit {Join, Split};

    Action(const JoinOrSplit t_type, const std::shared_ptr<Creature> t_creature,
           const std::shared_ptr<Creature> t_otherCreature);

    Action(const std::shared_ptr<Creature> t_creature, const Colour t_painterColour);

    Action(const std::shared_ptr<Creature> t_creature,
           const std::shared_ptr<Connection> t_connection);

    ActionType getType() const;

    std::shared_ptr<Creature> getCreature() const;
    std::shared_ptr<Creature> getOtherCreature() const;

    Colour getPainterColour() const;
    Colour getCreatureOldColour() const;
    Colour getCreatureNewColour() const;

    std::shared_ptr<Connection> getConnection() const;
    std::shared_ptr<Island> getStart() const;
    std::shared_ptr<Island> getDestination() const;

    bool operator < (const Action &t_action) const;

private:
    ActionType m_type;

    std::shared_ptr<Creature> m_creature;
    std::shared_ptr<Creature> m_otherCreature;

    Colour m_painter;
    Colour m_oldColour;

    std::shared_ptr<Connection> m_connection;
};

#endif // ACTION_H
