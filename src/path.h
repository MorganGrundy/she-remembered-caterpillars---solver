#ifndef PATH_H
#define PATH_H

#include <memory>
#include <vector>

#include "creature.h"
#include "action.h"

class Path
{
public:
    std::vector<Action> actions;
    std::vector<bool> switchBridgeState;
    std::vector<std::shared_ptr<Creature>> creatures;

    Path();
    Path(const Path &t_path);

    bool operator < (const Path &t_path) const;

    std::string toString() const;
    std::string toString(const std::vector<ActionType> &t_expected) const;
};

class SolverState
{
public:
    const std::vector<bool> switchBridgeState;
    const std::vector<std::shared_ptr<Creature>> creatures;

    SolverState();
    SolverState(const Path &t_path);

    bool operator < (const SolverState &t_state) const;
    size_t id;

private:
    static size_t nextID;
};

#endif // PATH_H
