#ifndef HEXAGONALDIRECTION_H
#define HEXAGONALDIRECTION_H

#include <vector>

enum class HexagonalDirection {North, NorthEast, SouthEast, South, SouthWest, NorthWest};
static const std::vector<HexagonalDirection> HexagonalDirectionVector {HexagonalDirection::North,
            HexagonalDirection::NorthEast, HexagonalDirection::SouthEast, HexagonalDirection::South,
            HexagonalDirection::SouthWest, HexagonalDirection::NorthWest};

#endif // HEXAGONALDIRECTION_H
