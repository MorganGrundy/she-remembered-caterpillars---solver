#include "imageloader.h"

#include <QString>

#include "colour.h"

const QString ImageLoader::switchBridgePath = ":/Images/Switch-bridge/Switch-bridge.png";
const QString ImageLoader::activatorPath = ":/Images/Activator-bridge/Activator.png";
const QString ImageLoader::destinationPath = ":/Images/Destination/Destination.png";

const QString ImageLoader::creaturePath = ":/Images/Creature/";
const QString ImageLoader::painterPath = ":/Images/Painter/";
const QString ImageLoader::bridgePath = ":/Images/Bridge/";
const QString ImageLoader::gatePath = ":/Images/Gate/";
const QString ImageLoader::activatorBridgePath = ":/Images/Activator-bridge/";

QString ImageLoader::loadCreature(Colour t_colour)
{
    return ImageLoader::creaturePath + QString::fromStdString(ColourHelper::toString(t_colour))
            + "Creature.png";
}

QString ImageLoader::loadPainter(Colour t_colour)
{
    return ImageLoader::painterPath + QString::fromStdString(ColourHelper::toString(t_colour))
            + "Painter.png";
}

QString ImageLoader::loadBridge(Colour t_colour)
{
    return ImageLoader::bridgePath + QString::fromStdString(ColourHelper::toString(t_colour))
            + "Bridge.png";
}

QString ImageLoader::loadGate(Colour t_colour)
{
    return ImageLoader::gatePath + QString::fromStdString(ColourHelper::toString(t_colour))
            + "Gate.png";
}

QString ImageLoader::loadActivatorBridge(bool t_active)
{
    return ImageLoader::activatorBridgePath + ((t_active) ? "Active.png" : "Inactive.png");
}
