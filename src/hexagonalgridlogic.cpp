#include "hexagonalgridlogic.h"

#include <QString>
#include <optional>
#include <vector>
#include <QFile>
#include <QDataStream>

#include "colour.h"
#include "hexagonaldirection.h"
#include "solver.h"

HexagonalGridLogic::HexagonalGridLogic() : debug{false}, m_hexagonCols{0}, m_hexagonRows{0},
    m_grid{}, m_connections{} {}

//Attempts to save the current grid state to a file of the given name
//Returns error string, if exists
QString HexagonalGridLogic::saveToFile(QString filename) const
{
    if (!filename.isEmpty())
    {
        QFile file(filename);
        if (!file.open(QIODevice::WriteOnly))
            return file.errorString();

        QDataStream out(&file);
        out.setVersion(QDataStream::Qt_5_12);

        //Save grid size
        out << QString::number(m_hexagonCols);
        out << QString::number(m_hexagonRows);

        //Save number of cells with islands
        size_t noIslandCell = 0;
        for (const auto &col: m_grid)
            for (auto cell: col)
                if (cell->isIsland())
                    ++noIslandCell;
        out << QString::number(noIslandCell);

        //Save cells with islands
        for (const auto &col: m_grid)
        {
            for (auto cell: col)
            {
                //Only care about island cells
                if (!cell->isIsland())
                    continue;

                out << QString::number(cell->pos.first);
                out << QString::number(cell->pos.second);
                out << QString::number(cell->hasDestination());
                out << QString::number(static_cast<int>(cell->hasCreature()));
                out << QString::number(static_cast<int>(cell->hasPainter()));

                //Save activator
                if (auto activator = cell->hasActivator().lock())
                {
                    const auto it = std::find_if(m_connections.begin(), m_connections.end(),
                                           [activator](auto c)
                                           {return c.lock() == activator;});
                    out << QString::number(std::distance(m_connections.begin(), it));
                }
                else
                    out << QString::number(-1);
            }
        }

        //Save connections
        out << QString::number(m_connections.size());

        for (const auto &weakConnection: m_connections)
        {
            const auto connection = weakConnection.lock();
            out << QString::number(static_cast<int>(connection->m_type));
            out << QString::number(static_cast<int>(connection->m_colour));

            out << QString::number(connection->getCellSize());
            for (size_t i = 0; i < connection->getCellSize(); ++i)
            {
                const auto cell = connection->getCellAt(i);
                out << QString::number(cell->pos.first);
                out << QString::number(cell->pos.second);
            }
        }
    }
    return "";
}

//Attempts to load a grid state from a file of the given name
//Returns error string, if exists
QString HexagonalGridLogic::loadFromFile(QString filename)
{
    if (!filename.isEmpty())
    {
        QFile file(filename);
        if (!file.open(QIODevice::ReadOnly))
            return file.errorString();

        QDataStream in(&file);
        in.setVersion(QDataStream::Qt_5_12);

        QString stringIn;

        //Read grid size
        size_t gridWidth, gridHeight;
        in >> stringIn;
        gridWidth = stringIn.toULongLong();
        in >> stringIn;
        gridHeight = stringIn.toULongLong();

        std::vector<std::vector<std::shared_ptr<HexagonalCell>>> grid(
                    gridWidth, std::vector<std::shared_ptr<HexagonalCell>>(gridHeight, nullptr));

        grid.resize(gridWidth);
        for (size_t x = 0; x < gridWidth; ++x)
        {
            auto col = &grid.at(x);
            col->resize(gridHeight);

            for (size_t y = 0; y < gridHeight; ++y)
            {
                col->at(y) = std::make_shared<HexagonalCell>();
                col->at(y)->pos = {x,y};
            }
        }

        //Read number of cells with islands
        size_t noCell;
        in >> stringIn;
        noCell = stringIn.toULongLong();

        //Read cells with islands
        std::map<std::shared_ptr<HexagonalCell>, size_t> activatorMap;
        while (noCell > 0)
        {
            //Read cell position
            size_t cellX, cellY;
            in >> stringIn;
            cellX = stringIn.toULongLong();
            in >> stringIn;
            cellY = stringIn.toULongLong();
            const auto cell = grid.at(cellX).at(cellY);
            cell->addIsland();

            //Read cell destination state
            unsigned int intIn;
            in >> stringIn;
            intIn = stringIn.toUInt();
            if (intIn == 1)
            {
                cell->addDestination();
                //Can ignore creature and painter
                in >> stringIn;
                in >> stringIn;
            }
            else
            {
                //Read cell creature state
                in >> stringIn;
                intIn = stringIn.toUInt();
                if (intIn != 0)
                {
                    cell->addCreature(ColourHelper::fromUInt(intIn));
                    //Can ignore painter
                    in >> stringIn;
                }
                else
                {
                    //Read cell painter state
                    in >> stringIn;
                    intIn = stringIn.toUInt();
                    if (intIn != 0)
                        cell->addPainter(ColourHelper::fromUInt(intIn));
                }
            }

            //Read activator state
            in >> stringIn;
            long long index = stringIn.toLongLong();
            if (index >= 0)
                activatorMap.emplace(cell, index);
            --noCell;
        }

        //Read connections
        size_t noConnection;
        in >> stringIn;
        noConnection = stringIn.toULongLong();
        std::vector<std::weak_ptr<HexagonalCellConnection>> connections;
        while (noConnection > 0)
        {
            ConnectionType type;
            in >> stringIn;
            type = static_cast<ConnectionType>(stringIn.toUInt());

            Colour colour;
            in >> stringIn;
            colour = ColourHelper::fromUInt(stringIn.toUInt());

            size_t cellCount;
            in >> stringIn;
            cellCount = stringIn.toULongLong();

            std::vector<std::shared_ptr<HexagonalCell>> cells;
            //Read cells in connection
            while (cellCount > 0)
            {
                //Read cell position
                size_t cellX, cellY;
                in >> stringIn;
                cellX = stringIn.toULongLong();
                in >> stringIn;
                cellY = stringIn.toULongLong();

                cells.push_back(grid.at(cellX).at(cellY));
                --cellCount;
            }

            const auto connection = std::make_shared<HexagonalCellConnection>(type, colour, cells);
            connection->initialise();
            connections.push_back(connection);

            --noConnection;
        }

        //Add activators to cells
        for (auto &pair: activatorMap)
            pair.first->addActivator(connections.at(pair.second));

        //Update grid
        m_grid = grid;
        m_connections = connections;
        m_hexagonCols = gridWidth;
        m_hexagonRows = gridHeight;
    }
    return "";
}

//Returns the cell in the given direction and distance from the given cell
std::optional<std::pair<size_t, size_t>> HexagonalGridLogic::findCell(
        const std::pair<size_t, size_t> from, const HexagonalDirection direction,
        const size_t distance) const
{
    std::pair<long long, long long> current = from;
    for (size_t i = 0; i < distance; ++i)
    {
        //Move in direction by one cell
        switch (direction)
        {
        case HexagonalDirection::North:
            current.second -= 1;
            break;
        case HexagonalDirection::South:
            current.second += 1;
            break;
        case HexagonalDirection::NorthEast:
            if (current.first % 2 == 0)
                current.second -= 1;
            current.first += 1;
            break;
        case HexagonalDirection::SouthEast:
            if (current.first % 2 == 1)
                current.second += 1;
            current.first += 1;
            break;
        case HexagonalDirection::SouthWest:
            if (current.first % 2 == 1)
                current.second += 1;
            current.first -= 1;
            break;
        case HexagonalDirection::NorthWest:
            if (current.first % 2 == 0)
                current.second -= 1;
            current.first -= 1;
            break;
        }
    }

    if (current.first < 0 || current.second < 0)
        return std::nullopt;
    else
        return std::pair<size_t, size_t>(current.first, current.second);
}

//Converts grid to a solver world and returns it
Solver HexagonalGridLogic::convertToSolver() const
{
    Solver world;
    world.debugMode = debug;

    //Iterate over cells grouping adjacent cells into islands
    std::vector<std::vector<size_t>> cellGroup(m_hexagonCols,
                                                     std::vector<size_t>(m_hexagonRows, 0));
    std::vector<std::vector<std::pair<size_t, size_t>>> groups(1);
    for (size_t x = 0; x < m_hexagonCols; ++x)
    {
        for (size_t y = 0; y < m_hexagonRows; ++y)
        {
            const std::shared_ptr<HexagonalCell> cell = m_grid.at(x).at(y);
            if (cell->isIsland())
            {
                std::set<size_t> merge;
                //Check adjacent cells for islands in a group
                for (const auto direction: HexagonalDirectionVector)
                {
                    const auto adjacent = findCell({x,y}, direction, 1);
                    if (adjacent
                            && adjacent->first < m_hexagonCols && adjacent->second < m_hexagonRows
                            && m_grid.at(adjacent->first).at(adjacent->second)->isIsland()
                            && cellGroup.at(adjacent->first).at(adjacent->second) != 0)
                    {
                        merge.insert(cellGroup.at(adjacent->first).at(adjacent->second));
                    }
                }

                if (merge.size() == 0)
                {
                    //Create new group and assign to cell
                    groups.push_back(std::vector<std::pair<size_t, size_t>>(1, {x,y}));
                    cellGroup.at(x).at(y) = groups.size()-1;
                }
                else
                {
                    //If cell has multiple adjacent group then merge them
                    size_t mainGroup = *merge.cbegin();
                    while (merge.size() > 1)
                    {
                        size_t groupToMerge = merge.extract(std::prev(merge.end())).value();
                        for (const auto &cellInGroup: groups.at(groupToMerge))
                            cellGroup.at(cellInGroup.first).at(cellInGroup.second) = mainGroup;
                        groups.at(mainGroup).insert(groups.at(mainGroup).end(),
                                                         groups.at(groupToMerge).begin(),
                                                         groups.at(groupToMerge).end());
                        groups.at(groupToMerge).clear();
                    }
                    //Assign cell to group
                    cellGroup.at(x).at(y) = mainGroup;
                    groups.at(mainGroup).push_back({x,y});
                }
            }
        }
    }

    //Remove first buffer object
    groups.erase(groups.begin(), groups.begin()+1);
    //Create Island from island cells
    std::map<std::shared_ptr<HexagonalCell>, std::shared_ptr<Island>> islandMap;
    for (const auto &cells: groups)
    {
        const auto island = std::make_shared<Island>();
        for (const auto &cell: cells)
        {
            island->setPainter(m_grid.at(cell.first).at(cell.second)->hasPainter(), true);

            if (m_grid.at(cell.first).at(cell.second)->hasDestination())
                ++island->destinations;

            if (m_grid.at(cell.first).at(cell.second)->hasCreature() != Colour::Invalid)
            {
                const auto creature = std::make_shared<Creature>(
                            m_grid.at(cell.first).at(cell.second)->hasCreature(), island);
                island->creatures.push_back(creature);
            }

            islandMap.emplace(m_grid.at(cell.first).at(cell.second), island);
        }
        world.islands.push_back(island);
    }

    //Create Connection from cell connections
    std::map<std::shared_ptr<HexagonalCellConnection>, std::shared_ptr<ActivatorBridge>> activatorMap;
    for (const auto &weakConnection: m_connections)
    {
        //Gets pointer and checks not null
        if (std::shared_ptr<HexagonalCellConnection> cellConnection = weakConnection.lock())
        {
            //Create connection
            std::shared_ptr<Connection> connection;
            switch (cellConnection->m_type)
            {
            case ConnectionType::Bridge:
                connection = std::make_shared<Bridge>(
                            islandMap.at(cellConnection->getCellAt(0)),
                            islandMap.at(cellConnection->getCellAt(cellConnection->getCellSize()-1)),
                            cellConnection->m_colour);
                break;
            case ConnectionType::Gate:
                connection = std::make_shared<Gate>(
                            islandMap.at(cellConnection->getCellAt(0)),
                            islandMap.at(cellConnection->getCellAt(cellConnection->getCellSize()-1)),
                            cellConnection->m_colour);
                break;
            case ConnectionType::SwitchBridge:
                connection = std::make_shared<SwitchBridge>(
                            islandMap.at(cellConnection->getCellAt(0)),
                            islandMap.at(cellConnection->getCellAt(cellConnection->getCellSize()-1)));
                break;
            case ConnectionType::ActivatorBridge:
            {
                const std::shared_ptr<ActivatorBridge> activatorBridge =
                        std::make_shared<ActivatorBridge>(
                            islandMap.at(cellConnection->getCellAt(0)),
                            islandMap.at(cellConnection->getCellAt(
                                             cellConnection->getCellSize()-1)));
                connection = activatorBridge;
                activatorMap.emplace(cellConnection, activatorBridge);
            }
                break;
            default:
                break;
            }
            connection->updateIslands();
            world.connections.push_back(connection);
        }
    }

    //Add activators to islands
    for (const auto &cells: groups)
    {
        for (const auto &cell: cells)
        {
            if (const std::shared_ptr<HexagonalCellConnection> activator
                    = m_grid.at(cell.first).at(cell.second)->hasActivator().lock())
            {
                const std::shared_ptr<ActivatorBridge> activatorBridge = activatorMap.at(activator);
                const std::shared_ptr<Island> island = islandMap.at(m_grid.at(cell.first).at(cell.second));

                island->activators.push_back(activatorBridge);
                const auto it = activatorBridge->activators.find(island);
                if (it == activatorBridge->activators.end())
                    activatorBridge->activators.emplace(island, 1);
                else
                    ++activatorBridge->activators.at(island);
            }
        }
    }

    return world;
}
