#include <QApplication>

#include "worlddesigner.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    WorldDesigner w;
    w.show();

    return app.exec();
}
