#include "path.h"

#include <memory>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

#include "connectiontype.h"
#include "connection.h"
#include "island.h"
#include "action.h"

Path::Path() {}

Path::Path(const Path &t_path)
    : actions{t_path.actions}, switchBridgeState{t_path.switchBridgeState}
{
    std::map<std::shared_ptr<Creature>, std::shared_ptr<Creature>> joinMap;
    for (auto p_creature: t_path.creatures)
    {
        std::shared_ptr<Creature> newCreature = std::make_shared<Creature>(*p_creature);
        creatures.push_back(newCreature);
        joinMap.emplace(p_creature, newCreature);
    }

    for (auto it = joinMap.cbegin(), end = joinMap.cend(); it != end; ++it)
        it->second->p_joinedCreature = joinMap[it->second->p_joinedCreature];
}

bool Path::operator < (const Path &t_path) const
{
    if (actions.size() < t_path.actions.size())
        return true;
    else if (actions.size() > t_path.actions.size())
        return false;
    else
        return (actions < t_path.actions);
}

std::string Path::toString() const
{
    std::string ss;
    ss += "Path size: ";
    ss += std::to_string(actions.size()) + '\n';
    for (size_t i = 0; i < actions.size(); ++i)
    {
        ss += "Action ";
        ss += std::to_string(i);
        ss += ": ";
        switch (actions.at(i).getType())
        {
        case ActionType::Join: ss += "Join"; break;
        case ActionType::Split: ss += "Split"; break;
        case ActionType::Painter: ss += "Use Painter"; break;
        case ActionType::Connection: ss += "Cross Connection ";
            switch(actions.at(i).getConnection()->type)
            {
            case ConnectionType::Bridge: ss += "Bridge"; break;
            case ConnectionType::Gate: ss += "Gate"; break;
            case ConnectionType::SwitchBridge: ss += "Switch Bridge"; break;
            case ConnectionType::ActivatorBridge: ss += "Activator Bridge"; break;
            default: ss += "Undefined"; break;
            }
            break;
        default: ss += "Undefined Action Type"; break;
        }
        ss += '\n';
    }
    return ss;
}

std::string Path::toString(const std::vector<ActionType> &t_expected) const
{
    std::string ss;
    ss += "Path size: ";
    ss += std::to_string(actions.size()) + " / " + std::to_string(t_expected.size()) + '\n';
    for (size_t i = 0; i < actions.size(); ++i)
    {
        ss += "Action ";
        ss += std::to_string(i);
        ss += ": ";
        switch (actions.at(i).getType())
        {
        case ActionType::Join: ss += "Join"; break;
        case ActionType::Split: ss += "Split"; break;
        case ActionType::Painter: ss += "Use Painter"; break;
        case ActionType::Connection: ss += "Cross Connection ";
            switch(actions.at(i).getConnection()->type)
            {
            case ConnectionType::Bridge: ss += "Bridge"; break;
            case ConnectionType::Gate: ss += "Gate"; break;
            case ConnectionType::SwitchBridge: ss += "Switch Bridge"; break;
            case ConnectionType::ActivatorBridge: ss += "Activator Bridge"; break;
            default: ss += "Undefined"; break;
            }
            break;
        default: ss += "Undefined Action Type"; break;
        }
        if (i < t_expected.size())
        {
            ss += " / ";
            switch (t_expected.at(i))
            {
            case ActionType::Join: ss += "Join"; break;
            case ActionType::Split: ss += "Split"; break;
            case ActionType::Painter: ss += "Use Painter"; break;
            case ActionType::Connection: ss += "Cross Connection "; break;
            default: ss += "Undefined Action Type"; break;
            }
        }
        ss += '\n';
    }
    return ss;
}

size_t SolverState::nextID = 0;

SolverState::SolverState() : id{nextID++} {}

SolverState::SolverState(const Path &t_path)
    : switchBridgeState{t_path.switchBridgeState}, creatures{t_path.creatures}, id{nextID++} {}

bool SolverState::operator < (const SolverState &t_state) const
{
    if (switchBridgeState < t_state.switchBridgeState)
        return true;
    else if (switchBridgeState > t_state.switchBridgeState)
        return false;
    else
    {
        std::vector<std::shared_ptr<Creature>> remainingCreature = creatures;
        std::vector<std::shared_ptr<Creature>> remainingOtherCreature = t_state.creatures;

        //For every creature look for an equivalent creature in t_state
        for (size_t i = 0; i < remainingCreature.size(); ++i)
        {
            auto creature = remainingCreature.at(i);
            if (creature)
            {
                for (size_t i2 = 0; i2 < remainingOtherCreature.size(); ++i2)
                {
                    auto creature2 = remainingOtherCreature.at(i2);
                    if (creature2)
                    {
                        if (creature->p_location == creature2->p_location)
                        {
                            if (creature->p_joinedCreature && creature2->p_joinedCreature &&
                                    creature->joinedColour == creature2->joinedColour)
                            {
                                //Match found, remove creature and joined creature
                                remainingCreature.at(i) = nullptr;
                                remainingOtherCreature.at(i2) = nullptr;

                                auto joinedIt = std::find(remainingCreature.cbegin(),
                                                          remainingCreature.cend(),
                                                          creature->p_joinedCreature);
                                size_t joinedIndex = static_cast<size_t>(
                                            std::distance(remainingCreature.cbegin(), joinedIt));
                                remainingCreature.at(joinedIndex) = nullptr;

                                joinedIt = std::find(remainingOtherCreature.cbegin(),
                                                          remainingOtherCreature.cend(),
                                                          creature2->p_joinedCreature);
                                joinedIndex = static_cast<size_t>(
                                            std::distance(remainingOtherCreature.cbegin(),
                                                          joinedIt));
                                remainingOtherCreature.at(joinedIndex) = nullptr;
                                break;
                            }
                            else if (!creature->p_joinedCreature && !creature2->p_joinedCreature &&
                                     creature->colour == creature2->colour)
                            {
                                //Match found, remove creature
                                remainingCreature.at(i) = nullptr;
                                remainingOtherCreature.at(i2) = nullptr;
                                break;
                            }
                        }
                    }
                }
            }
        }

        //Any creatures not matched just compare
        auto creature = remainingCreature.front();
        auto creature2 = remainingOtherCreature.front();
        if (creature && creature2)
        {
            if (creature->p_location->id < creature2->p_location->id)
                return true;
            else if (creature->p_location->id > creature2->p_location->id)
                return false;
            else if (creature->p_joinedCreature && creature2->p_joinedCreature)
            {
                if (creature->joinedColour < creature2->joinedColour)
                    return true;
                else if (creature->joinedColour > creature2->joinedColour)
                    return false;
            }
            else if (!creature->p_joinedCreature && !creature2->p_joinedCreature)
            {
                if (creature->colour < creature2->colour)
                    return true;
                else if (creature->colour > creature2->colour)
                    return false;
            }
            else if (!creature->p_joinedCreature && creature2->p_joinedCreature)
                return true;
            else if (creature->p_joinedCreature && !creature2->p_joinedCreature)
                return false;
        }

        return false;
    }
}
