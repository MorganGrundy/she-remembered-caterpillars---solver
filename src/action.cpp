#include "action.h"

#include <memory>

#include "creature.h"
#include "connection.h"

bool Action::operator <(const Action &t_action) const
{
    if (m_type < t_action.m_type)
        return true;
    else if (m_type > t_action.m_type)
        return false;
    else
    {
        switch (m_type)
        {
        case ActionType::Join: case ActionType::Split:
            if (m_creature->id < t_action.getCreature()->id)
                return true;
            else if (m_creature->id > t_action.getCreature()->id)
                return false;
            else
                return (m_otherCreature->id < t_action.getOtherCreature()->id);

        case ActionType::Painter:
            if (m_creature->id < t_action.getCreature()->id)
                return true;
            else if (m_creature->id > t_action.getCreature()->id)
                return false;
            else if (m_painter < t_action.getPainterColour())
                return true;
            else if (m_painter > t_action.getPainterColour())
                return false;
            else
                return (m_oldColour < t_action.getCreatureOldColour());

        case ActionType::Connection:
            if (m_creature->id < t_action.getCreature()->id)
                return true;
            else if (m_creature->id > t_action.getCreature()->id)
                return false;
            else
                return (m_connection->id < t_action.getConnection()->id);

        default:
            return false;
        }
    }
}

Action::Action(const JoinOrSplit t_type, const std::shared_ptr<Creature> t_creature,
               const std::shared_ptr<Creature> t_otherCreature)
    : m_type{(t_type == JoinOrSplit::Join) ? ActionType::Join : ActionType::Split},
      m_creature{t_creature}, m_otherCreature{t_otherCreature},
      m_painter{Colour::Invalid}, m_oldColour{Colour::Invalid}
{
    if (m_type == ActionType::Join)
    {
        if (!m_creature->join(m_otherCreature))
             throw std::invalid_argument("Creatures failed to join");
    }
    else if (!m_creature->split())
        throw std::invalid_argument("Creatures failed to split");
}

Action::Action(const std::shared_ptr<Creature> t_creature, const Colour t_painterColour)
    : m_type{ActionType::Painter}, m_creature{t_creature},
      m_painter{t_painterColour}, m_oldColour{t_creature->colour}
{
    if (m_creature->colour == Colour::Uncoloured)
        m_creature->colour = m_painter;
    else if (m_creature->colour == m_painter)
        m_creature->colour = Colour::Uncoloured;
    else
        throw std::invalid_argument("Creature failed to use painter");
}

Action::Action(const std::shared_ptr<Creature> t_creature,
               const std::shared_ptr<Connection> t_connection)
    : m_type{ActionType::Connection}, m_creature{t_creature},
      m_painter{Colour::Invalid}, m_oldColour{Colour::Invalid}, m_connection{t_connection}
{
    if (m_creature->p_location != m_connection->p_island1 &&
        m_creature->p_location != m_connection->p_island2)
             throw std::invalid_argument("Creature was not near Connection");

    std::shared_ptr<Island> destination = (m_creature->p_location == m_connection->p_island1) ?
            m_connection->p_island2 : m_connection->p_island1;
    m_creature->p_location = destination;
    if (m_creature->p_joinedCreature)
        m_creature->p_joinedCreature->p_location = destination;
}

ActionType Action::getType() const
{
    return m_type;
}

std::shared_ptr<Creature> Action::getCreature() const
{
    return m_creature;
}

std::shared_ptr<Creature> Action::getOtherCreature() const
{
    return m_otherCreature;
}

Colour Action::getPainterColour() const
{
    return m_painter;
}

Colour Action::getCreatureOldColour() const
{
    return m_oldColour;
}

Colour Action::getCreatureNewColour() const
{
    return m_creature->colour;
}

std::shared_ptr<Connection> Action::getConnection() const
{
    return m_connection;
}

std::shared_ptr<Island> Action::getStart() const
{
    return (m_creature->p_location == m_connection->p_island1) ?
                 m_connection->p_island2 : m_connection->p_island1;
}

std::shared_ptr<Island> Action::getDestination() const
{
    return m_creature->p_location;
}
