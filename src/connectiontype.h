#ifndef CONNECTIONTYPE_H
#define CONNECTIONTYPE_H

enum class ConnectionType {Undefined, Bridge, Gate, SwitchBridge, ActivatorBridge};

#endif // CONNECTIONTYPE_H
