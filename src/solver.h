#ifndef SOLVER_H
#define SOLVER_H

#include <optional>
#include <vector>
#include <set>
#include <memory>

#include "connection.h"
#include "island.h"
#include "path.h"

class Solver
{
public:
    Solver();
    std::optional<Path> run(const size_t t_maxActions = 100,
                            const size_t t_maxPaths = 10000);

    std::vector<std::shared_ptr<Island>> islands;
    std::vector<std::shared_ptr<Connection>> connections;

    bool debugMode = false;

private:
    std::set<Path> paths;
    std::set<SolverState> states;

    void addPath(const Path &t_path);
    Path createInitialPath(const std::vector<std::shared_ptr<SwitchBridge>> &t_switchBridges) const;

    bool pathIsValid(const Path &t_path,
                     const std::vector<std::shared_ptr<Island>> &t_destinations) const;

    void joinActions(const Path &t_currentPath);
    void splitActions(const Path &t_currentPath);
    void painterActions(const Path &t_currentPath);
    void connectionActions(const Path &t_currentPath,
                           const std::vector<std::shared_ptr<SwitchBridge>> &t_switchBridges);
    Path createConnectionAction(const Path &t_currentPath, const size_t t_creature,
                                const std::shared_ptr<Connection> t_connection) const;
    void switchBridgeAction();
};

#endif // SOLVER_H
