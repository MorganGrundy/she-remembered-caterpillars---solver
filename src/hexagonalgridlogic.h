#ifndef HEXAGONALGRIDLOGIC_H
#define HEXAGONALGRIDLOGIC_H

#include <QString>
#include <optional>
#include <vector>
#include <memory>

#include "hexagonaldirection.h"
#include "hexagonalcellconnection.h"
#include "hexagonalcell.h"
#include "solver.h"

class HexagonalGridLogic
{
public:
    HexagonalGridLogic();

    QString saveToFile(QString filename) const;
    QString loadFromFile(QString filename);

    std::optional<std::pair<size_t, size_t>> findCell(const std::pair<size_t, size_t> from,
                                 const HexagonalDirection direction, const size_t distance) const;

    Solver convertToSolver() const;

    bool debug;

    //Grid
    size_t m_hexagonCols, m_hexagonRows;
    std::vector<std::vector<std::shared_ptr<HexagonalCell>>> m_grid;
    std::vector<std::weak_ptr<HexagonalCellConnection>> m_connections;
};

#endif // HEXAGONALGRIDLOGIC_H
