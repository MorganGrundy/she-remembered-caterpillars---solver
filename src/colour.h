#ifndef COLOUR_H
#define COLOUR_H

#include <string>
#include <vector>
#include <optional>

//Base colours = (Red, Yellow, Blue), Composite colours = (Orange, Purple, Green)
enum class Colour {Invalid = 0, Uncoloured = 1, Red = 2, Yellow = 3, Blue = 4, Orange = 5,
                    Purple = 6, Green = 7};

class ColourHelper
{
public:
    static const std::vector<Colour> Colours;
    static const std::vector<Colour>::const_iterator baseBegin;
    static const std::vector<Colour>::const_iterator baseEnd;
    static const std::vector<Colour>::const_iterator compositeBegin;
    static const std::vector<Colour>::const_iterator compositeEnd;

    static Colour combine(const Colour t_col1, const Colour t_col2);
    static std::optional<std::pair<Colour,Colour>> split(const Colour t_colour);

    static Colour fromUInt(const unsigned int t_index);

    static std::string toString(const Colour t_colour);
};

#endif // COLOUR_H
