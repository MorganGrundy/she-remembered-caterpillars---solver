#include "creature.h"

#include <memory>

#include "colour.h"
#include "island.h"

size_t Creature::nextID = 0;

//Constructor
Creature::Creature(const Colour t_colour, const std::shared_ptr<Island> t_location)
    : colour{t_colour}, p_location{t_location}, b_main{false}, joinedColour{Colour::Invalid},
      id{nextID++} {}

//Returns if a creature can be split
bool Creature::canSplit() const
{
    return (p_joinedCreature && b_main);
}

//Returns if two creatures can join
bool Creature::canJoin(const std::shared_ptr<Creature> t_creature) const
{
    if (p_location != t_creature->p_location || p_joinedCreature || t_creature->p_joinedCreature)
        return false;

    return (ColourHelper::combine(colour, t_creature->colour) != Colour::Invalid);
}

//Splits a joint creature
//Returns whether it was successful
bool Creature::split()
{
    if (!p_joinedCreature || !b_main)
        return false;
    else
    {
        p_joinedCreature->p_joinedCreature = nullptr;
        p_joinedCreature = nullptr;
        b_main = false;
        return true;
    }
}

//Attempts to join two creatures
//Returns whether it was successful
bool Creature::join(const std::shared_ptr<Creature> t_creature)
{
    if (p_location != t_creature->p_location || p_joinedCreature || t_creature->p_joinedCreature)
        return false;

    Colour newColour = ColourHelper::combine(colour, t_creature->colour);
    if (newColour != Colour::Invalid)
    {
        p_joinedCreature = t_creature;
        t_creature->p_joinedCreature = shared_from_this();
        joinedColour = newColour;
        b_main = true;

        return true;
    }
    else
        return false;
}
