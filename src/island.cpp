#include "island.h"

#include "colour.h"

size_t Island::nextID = 0;

Island::Island()
    : id{nextID++} {}

//Returns true if painter of given colour exists on this island
bool Island::getPainter(const Colour t_colour) const
{
    switch (t_colour)
    {
    case Colour::Red: return b_redPainter;
    case Colour::Yellow: return b_yellowPainter;
    case Colour::Blue: return b_bluePainter;
    case Colour::Uncoloured: return b_redPainter || b_yellowPainter || b_bluePainter;
    default: return false;
    }
}

void Island::setPainter(const Colour t_colour, const bool state)
{
    switch (t_colour)
    {
    case Colour::Red: b_redPainter = state; break;
    case Colour::Yellow: b_yellowPainter = state; break;
    case Colour::Blue: b_bluePainter = state; break;
    default: break;
    }
}
