#ifndef CONNECTION_H
#define CONNECTION_H

#include <map>
#include <memory>

#include "colour.h"
#include "connectiontype.h"
#include "creature.h"

class Island;

class Connection : public std::enable_shared_from_this<Connection>
{
public:
    //Constructors
    Connection();
    Connection(const std::shared_ptr<Island> t_island1, const std::shared_ptr<Island> t_island2,
               const ConnectionType t_type);

    void updateIslands();

    std::shared_ptr<Island> p_island1; //Pointer to island 1
    std::shared_ptr<Island> p_island2; //Pointer to island 2
    const ConnectionType type; //Type of connection

    size_t id;

private:
    static size_t nextID;
};

class Bridge : public Connection
{
public:
    Bridge(const std::shared_ptr<Island> t_island1, const std::shared_ptr<Island> t_island2,
           const Colour t_colour);
    bool canCross(const std::shared_ptr<Creature> t_creature) const;

    Colour colour;
};

class Gate : public Connection
{
public:
    Gate(const std::shared_ptr<Island> t_island1, const std::shared_ptr<Island> t_island2,
         const Colour t_colour);
    bool canCross(const std::shared_ptr<Creature> t_creature) const;

    Colour colour;
};

class SwitchBridge : public Connection
{
public:
    SwitchBridge(const std::shared_ptr<Island> t_island1, const std::shared_ptr<Island> t_island2);
    bool canCross(const std::shared_ptr<Island> t_island, const Colour t_creatureColour) const;
};

class ActivatorBridge : public Connection
{
public:
    ActivatorBridge(const std::shared_ptr<Island> t_island1,
                    const std::shared_ptr<Island> t_island2);

    //List of pointers to islands that have activators and counts
    std::map<std::shared_ptr<Island>, int> activators;
};

#endif // CONNECTION_H
